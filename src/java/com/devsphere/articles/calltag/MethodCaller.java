package com.devsphere.articles.calltag;

import com.devsphere.articles.calltag.jstl.Coercions;
import com.devsphere.articles.calltag.jstl.ELException;
import com.devsphere.articles.calltag.jstl.Logger;
import javax.servlet.jsp.JspException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;

public class MethodCaller {

    private static Method findMethod(
            Object object, String methodName, int paramCount)
            throws JspException {
        Class clazz = object.getClass();
        Method pubMethods[] = clazz.getMethods();
        for (int i = 0; i < pubMethods.length; i++) {
            Method m = pubMethods[i];
            if (methodName.equals(m.getName())
                    && m.getParameterTypes().length == paramCount)
                return m;
        }
        throw new JspException("Method not found: "
            + clazz.getName() + "." + methodName + "()");
    }

    private static Object[] coerceParamValuesToParamTypes(
            Method method, List paramValues)
            throws JspException {
        Class paramTypes[] = method.getParameterTypes();
        Object coercedValues[] = new Object[paramTypes.length];
        Iterator paramIterator = paramValues.iterator();
        Logger logger = new Logger(System.err);
        for (int i = 0; i < paramTypes.length; i++) {
            Object paramValue = paramIterator.next();
            Class paramClass = paramTypes[i];
            if (paramValue == null || paramValue.getClass() != paramClass)
                try {
                    paramValue = Coercions.coerce(
                        paramValue, paramClass, logger);
                } catch (ELException e) {
                    throw new JspException(e.getMessage(), e.getRootCause());
                }
            coercedValues[i] = paramValue;
        }
        return coercedValues;
    }

    public static Object call(
            Object object, String methodName, List paramValues)
            throws JspException {
        Method method = findMethod(object, methodName, paramValues.size());
        Object args[] = coerceParamValuesToParamTypes(method, paramValues);
        try {
            return method.invoke(object, args);
        } catch (InvocationTargetException e) {
            throw new JspException(e.getTargetException());
        } catch (IllegalAccessException e) {
            throw new JspException(e);
        } catch (IllegalArgumentException e) {
            throw new JspException(e);
        }
    }

}
