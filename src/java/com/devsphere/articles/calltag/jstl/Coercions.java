/*
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 1999 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "The Jakarta Project", "Tomcat", and "Apache Software
 *    Foundation" must not be used to endorse or promote products derived
 *    from this software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache"
 *    nor may "Apache" appear in their names without prior written
 *    permission of the Apache Group.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

package com.devsphere.articles.calltag.jstl;
/// package org.apache.taglibs.standard.lang.jstl;

import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;

/**
 *
 * <p>This class contains the logic for coercing data types before
 * operators are applied to them.
 *
 * <p>The following is the list of rules applied for various type
 * conversions.
 *
 * <pre>
 * coercions
 *
 *   coerce A to String
 *     A is String
 *       return A
 *     A is null
 *       return ""
 *     A.toString throws exception
 *       error
 *     otherwise
 *       return A.toString
 *
 *   coerce A to primitive Number type N
 *     A is null or ""
 *       return 0
 *     A is Character
 *       convert to short, apply following rules
 *     A is Boolean
 *       error
 *     A is Number type N
 *       return A
 *     A is Number with less precision than N
 *       coerce quietly
 *     A is Number with greater precision than N
 *       coerce quietly
 *     A is String
 *       new N.valueOf(A) throws exception
 *         error
 *       return N.valueOf(A)
 *     otherwise
 *       error
 *
 *   coerce A to Character should be
 *     A is null or ""
 *       return (char) 0
 *     A is Character
 *       return A
 *     A is Boolean
 *       error
 *     A is Number with less precision than short
 *       coerce quietly - return (char) A
 *     A is Number with greater precision than short
 *       coerce quietly - return (char) A
 *     A is String
 *       return A.charAt (0)
 *     otherwise
 *       error
 *
 *   coerce A to Boolean
 *     A is null or ""
 *       return false
 *     A is Boolean
 *       return A
 *     A is String
 *       Boolean.valueOf(A) throws exception
 *         error
 *       return Boolean.valueOf(A)
 *     otherwise
 *       error
 *
 *   coerce A to any other type T
 *     A is null
 *       return null
 *     A is assignable to T
 *       coerce quietly
 *     A is String
 *       T has no PropertyEditor
 *         if A is "", return null
 *         otherwise error
 *       T's PropertyEditor throws exception
 *         if A is "", return null
 *         otherwise error
 *       otherwise
 *         apply T's PropertyEditor
 *     otherwise
 *       error
 * </pre>
 *
 * @author Nathan Abramson - Art Technology Group
 * @version $Change: 181177 $$DateTime: 2001/06/26 08:45:09 $$Author: arista $
 **/

public class Coercions
{
  //-------------------------------------
  /**
   *
   * Coerces the given value to the specified class.
   **/
  public static Object coerce (Object pValue,
			       Class pClass,
			       Logger pLogger)
    throws ELException
  {
    if (pClass == String.class) {
      return coerceToString (pValue, pLogger);
    }
    else if (isPrimitiveNumberClass (pClass)) {
      return coerceToPrimitiveNumber (pValue, pClass, pLogger);
    }
    else if (pClass == Character.class ||
	     pClass == Character.TYPE) {
      return coerceToCharacter (pValue, pLogger);
    }
    else if (pClass == Boolean.class ||
	     pClass == Boolean.TYPE) {
      return coerceToBoolean (pValue, pLogger);
    }
    else {
      return coerceToObject (pValue, pClass, pLogger);
    }
  }

  //-------------------------------------
  /**
   *
   * Returns true if the given class is Byte, Short, Integer, Long,
   * Float, Double
   **/
  static boolean isPrimitiveNumberClass (Class pClass)
  {
    return
      pClass == Byte.class ||
      pClass == Byte.TYPE ||
      pClass == Short.class ||
      pClass == Short.TYPE ||
      pClass == Integer.class ||
      pClass == Integer.TYPE ||
      pClass == Long.class ||
      pClass == Long.TYPE ||
      pClass == Float.class ||
      pClass == Float.TYPE ||
      pClass == Double.class ||
      pClass == Double.TYPE;
  }

  //-------------------------------------
  /**
   *
   * Coerces the specified value to a String
   **/
  public static String coerceToString (Object pValue,
				       Logger pLogger)
    throws ELException
  {
    if (pValue == null) {
      return "";
    }
    else if (pValue instanceof String) {
      return (String) pValue;
    }
    else {
      try {
	return pValue.toString ();
      }
      catch (Exception exc) {
	if (pLogger.isLoggingError ()) {
	  pLogger.logError (Constants.TOSTRING_EXCEPTION,
			    exc,
			    pValue.getClass ().getName ());
	}
	return "";
      }
    }
  }

  //-------------------------------------
  /**
   *
   * Coerces a value to the given primitive number class
   **/
  public static Number coerceToPrimitiveNumber (Object pValue,
						Class pClass,
						Logger pLogger)
    throws ELException
  {
    if (pValue == null ||
	"".equals (pValue)) {
      return coerceToPrimitiveNumber (0, pClass);
    }
    else if (pValue instanceof Character) {
      char val = ((Character) pValue).charValue ();
      return coerceToPrimitiveNumber ((short) val, pClass);
    }
    else if (pValue instanceof Boolean) {
      if (pLogger.isLoggingError ()) {
	pLogger.logError (Constants.BOOLEAN_TO_NUMBER,
			  pValue,
			  pClass.getName ());
      }
      return coerceToPrimitiveNumber (0, pClass);
    }
    else if (pValue.getClass () == pClass) {
      return (Number) pValue;
    }
    else if (pValue instanceof Number) {
      return coerceToPrimitiveNumber ((Number) pValue, pClass);
    }
    else if (pValue instanceof String) {
      try {
	return coerceToPrimitiveNumber ((String) pValue, pClass);
      }
      catch (Exception exc) {
	if (pLogger.isLoggingError ()) {
	  pLogger.logError
	    (Constants.STRING_TO_NUMBER_EXCEPTION,
	     (String) pValue,
	     pClass.getName ());
	}
	return coerceToPrimitiveNumber (0, pClass);
      }
    }
    else {
      if (pLogger.isLoggingError ()) {
	pLogger.logError
	  (Constants.COERCE_TO_NUMBER,
	   pValue.getClass ().getName (),
	   pClass.getName ());
      }
      return coerceToPrimitiveNumber (0, pClass);
    }
  }

  //-------------------------------------
  /**
   *
   * Coerces a value to an Integer, returning null if the coercion
   * isn't possible.
   **/
  public static Integer coerceToInteger (Object pValue,
					 Logger pLogger)
    throws ELException
  {
    if (pValue == null) {
      return null;
    }
    else if (pValue instanceof Character) {
      return PrimitiveObjects.getInteger
	((int) (((Character) pValue).charValue ()));
    }
    else if (pValue instanceof Boolean) {
      if (pLogger.isLoggingWarning ()) {
	pLogger.logWarning (Constants.BOOLEAN_TO_NUMBER,
			    pValue,
			    Integer.class.getName ());
      }
      return PrimitiveObjects.getInteger
	(((Boolean) pValue).booleanValue () ? 1 : 0);
    }
    else if (pValue instanceof Integer) {
      return (Integer) pValue;
    }
    else if (pValue instanceof Number) {
      return PrimitiveObjects.getInteger (((Number) pValue).intValue ());
    }
    else if (pValue instanceof String) {
      try {
	return Integer.valueOf ((String) pValue);
      }
      catch (Exception exc) {
	if (pLogger.isLoggingWarning ()) {
	  pLogger.logWarning
	    (Constants.STRING_TO_NUMBER_EXCEPTION,
	     (String) pValue,
	     Integer.class.getName ());
	}
	return null;
      }
    }
    else {
      if (pLogger.isLoggingWarning ()) {
	pLogger.logWarning
	  (Constants.COERCE_TO_NUMBER,
	   pValue.getClass ().getName (),
	   Integer.class.getName ());
      }
      return null;
    }
  }

  //-------------------------------------
  /**
   *
   * Coerces a long to the given primitive number class
   **/
  static Number coerceToPrimitiveNumber (long pValue,
					 Class pClass)
    throws ELException
  {
    if (pClass == Byte.class || pClass == Byte.TYPE) {
      return PrimitiveObjects.getByte ((byte) pValue);
    }
    else if (pClass == Short.class || pClass == Short.TYPE) {
      return PrimitiveObjects.getShort ((short) pValue);
    }
    else if (pClass == Integer.class || pClass == Integer.TYPE) {
      return PrimitiveObjects.getInteger ((int) pValue);
    }
    else if (pClass == Long.class || pClass == Long.TYPE) {
      return PrimitiveObjects.getLong ((long) pValue);
    }
    else if (pClass == Float.class || pClass == Float.TYPE) {
      return PrimitiveObjects.getFloat ((float) pValue);
    }
    else if (pClass == Double.class || pClass == Double.TYPE) {
      return PrimitiveObjects.getDouble ((double) pValue);
    }
    else {
      return PrimitiveObjects.getInteger (0);
    }
  }

  //-------------------------------------
  /**
   *
   * Coerces a double to the given primitive number class
   **/
  static Number coerceToPrimitiveNumber (double pValue,
					 Class pClass)
    throws ELException
  {
    if (pClass == Byte.class || pClass == Byte.TYPE) {
      return PrimitiveObjects.getByte ((byte) pValue);
    }
    else if (pClass == Short.class || pClass == Short.TYPE) {
      return PrimitiveObjects.getShort ((short) pValue);
    }
    else if (pClass == Integer.class || pClass == Integer.TYPE) {
      return PrimitiveObjects.getInteger ((int) pValue);
    }
    else if (pClass == Long.class || pClass == Long.TYPE) {
      return PrimitiveObjects.getLong ((long) pValue);
    }
    else if (pClass == Float.class || pClass == Float.TYPE) {
      return PrimitiveObjects.getFloat ((float) pValue);
    }
    else if (pClass == Double.class || pClass == Double.TYPE) {
      return PrimitiveObjects.getDouble ((double) pValue);
    }
    else {
      return PrimitiveObjects.getInteger (0);
    }
  }

  //-------------------------------------
  /**
   *
   * Coerces a Number to the given primitive number class
   **/
  static Number coerceToPrimitiveNumber (Number pValue,
					 Class pClass)
    throws ELException
  {
    if (pClass == Byte.class || pClass == Byte.TYPE) {
      return PrimitiveObjects.getByte (pValue.byteValue ());
    }
    else if (pClass == Short.class || pClass == Short.TYPE) {
      return PrimitiveObjects.getShort (pValue.shortValue ());
    }
    else if (pClass == Integer.class || pClass == Integer.TYPE) {
      return PrimitiveObjects.getInteger (pValue.intValue ());
    }
    else if (pClass == Long.class || pClass == Long.TYPE) {
      return PrimitiveObjects.getLong (pValue.longValue ());
    }
    else if (pClass == Float.class || pClass == Float.TYPE) {
      return PrimitiveObjects.getFloat (pValue.floatValue ());
    }
    else if (pClass == Double.class || pClass == Double.TYPE) {
      return PrimitiveObjects.getDouble (pValue.doubleValue ());
    }
    else {
      return PrimitiveObjects.getInteger (0);
    }
  }

  //-------------------------------------
  /**
   *
   * Coerces a String to the given primitive number class
   **/
  static Number coerceToPrimitiveNumber (String pValue,
					 Class pClass)
    throws ELException
  {
    if (pClass == Byte.class || pClass == Byte.TYPE) {
      return Byte.valueOf (pValue);
    }
    else if (pClass == Short.class || pClass == Short.TYPE) {
      return Short.valueOf (pValue);
    }
    else if (pClass == Integer.class || pClass == Integer.TYPE) {
      return Integer.valueOf (pValue);
    }
    else if (pClass == Long.class || pClass == Long.TYPE) {
      return Long.valueOf (pValue);
    }
    else if (pClass == Float.class || pClass == Float.TYPE) {
      return Float.valueOf (pValue);
    }
    else if (pClass == Double.class || pClass == Double.TYPE) {
      return Double.valueOf (pValue);
    }
    else {
      return PrimitiveObjects.getInteger (0);
    }
  }

  //-------------------------------------
  /**
   *
   * Coerces a value to a Character
   **/
  public static Character coerceToCharacter (Object pValue,
					     Logger pLogger)
    throws ELException
  {
    if (pValue == null ||
	"".equals (pValue)) {
      return PrimitiveObjects.getCharacter ((char) 0);
    }
    else if (pValue instanceof Character) {
      return (Character) pValue;
    }
    else if (pValue instanceof Boolean) {
      if (pLogger.isLoggingError ()) {
	pLogger.logError (Constants.BOOLEAN_TO_CHARACTER, pValue);
      }
      return PrimitiveObjects.getCharacter ((char) 0);
    }
    else if (pValue instanceof Number) {
      return PrimitiveObjects.getCharacter
	((char) ((Number) pValue).shortValue ());
    }
    else if (pValue instanceof String) {
      String str = (String) pValue;
      return PrimitiveObjects.getCharacter (str.charAt (0));
    }
    else {
      if (pLogger.isLoggingError ()) {
	pLogger.logError
	  (Constants.COERCE_TO_CHARACTER,
	   pValue.getClass ().getName ());
      }
      return PrimitiveObjects.getCharacter ((char) 0);
    }
  }

  //-------------------------------------
  /**
   *
   * Coerces a value to a Boolean
   **/
  public static Boolean coerceToBoolean (Object pValue,
					 Logger pLogger)
    throws ELException
  {
    if (pValue == null ||
	"".equals (pValue)) {
      return Boolean.FALSE;
    }
    else if (pValue instanceof Boolean) {
      return (Boolean) pValue;
    }
    else if (pValue instanceof String) {
      String str = (String) pValue;
      try {
	return Boolean.valueOf (str);
      }
      catch (Exception exc) {
	if (pLogger.isLoggingError ()) {
	  pLogger.logError
	    (Constants.STRING_TO_BOOLEAN,
	     exc,
	     (String) pValue);
	}
	return Boolean.FALSE;
      }
    }
    else {
      if (pLogger.isLoggingError ()) {
	pLogger.logError
	  (Constants.COERCE_TO_BOOLEAN,
	   pValue.getClass ().getName ());
      }
      return Boolean.TRUE;
    }
  }

  //-------------------------------------
  /**
   *
   * Coerces a value to the specified Class that is not covered by any
   * of the above cases
   **/
  public static Object coerceToObject (Object pValue,
				       Class pClass,
				       Logger pLogger)
    throws ELException
  {
    if (pValue == null) {
      return null;
    }
    else if (pClass.isAssignableFrom (pValue.getClass ())) {
      return pValue;
    }
    else if (pValue instanceof String) {
      String str = (String) pValue;
      PropertyEditor pe = PropertyEditorManager.findEditor (pClass);
      if (pe == null) {
	if ("".equals (str)) {
	  return null;
	}
	else {
	  if (pLogger.isLoggingError ()) {
	    pLogger.logError
	      (Constants.NO_PROPERTY_EDITOR,
	       str,
	       pClass.getName ());
	  }
	  return null;
	}
      }
      try {
	pe.setAsText (str);
	return pe.getValue ();
      }
      catch (IllegalArgumentException exc) {
	if ("".equals (str)) {
	  return null;
	}
	else {
	  if (pLogger.isLoggingError ()) {
	    pLogger.logError
	      (Constants.PROPERTY_EDITOR_ERROR,
	       exc,
	       pValue,
	       pClass.getName ());
	  }
	  return null;
	}
      }
    }
    else {
      if (pLogger.isLoggingError ()) {
	pLogger.logError
	  (Constants.COERCE_TO_OBJECT,
	   pValue.getClass ().getName (),
	   pClass.getName ());
      }
      return null;
    }
  }

  //-------------------------------------
  /**
   *
   * Returns true if the given Object is of a floating point type
   **/
  public static boolean isFloatingPointType (Object pObject)
  {
    return
      pObject != null &&
      isFloatingPointType (pObject.getClass ());
  }

  //-------------------------------------
  /**
   *
   * Returns true if the given class is of a floating point type
   **/
  public static boolean isFloatingPointType (Class pClass)
  {
    return
      pClass == Float.class ||
      pClass == Float.TYPE ||
      pClass == Double.class ||
      pClass == Double.TYPE;
  }

  //-------------------------------------
  /**
   *
   * Returns true if the given string might contain a floating point
   * number - i.e., it contains ".", "e", or "E"
   **/
  public static boolean isFloatingPointString (Object pObject)
  {
    if (pObject instanceof String) {
      String str = (String) pObject;
      int len = str.length ();
      for (int i = 0; i < len; i++) {
	char ch = str.charAt (i);
	if (ch == '.' ||
	    ch == 'e' ||
	    ch == 'E') {
	  return true;
	}
      }
      return false;
    }
    else {
      return false;
    }
  }

  //-------------------------------------
  /**
   *
   * Returns true if the given Object is of an integer type
   **/
  public static boolean isIntegerType (Object pObject)
  {
    return
      pObject != null &&
      isIntegerType (pObject.getClass ());
  }

  //-------------------------------------
  /**
   *
   * Returns true if the given class is of an integer type
   **/
  public static boolean isIntegerType (Class pClass)
  {
    return
      pClass == Byte.class ||
      pClass == Byte.TYPE ||
      pClass == Short.class ||
      pClass == Short.TYPE ||
      pClass == Character.class ||
      pClass == Character.TYPE ||
      pClass == Integer.class ||
      pClass == Integer.TYPE ||
      pClass == Long.class ||
      pClass == Long.TYPE;
  }

  //-------------------------------------

}
