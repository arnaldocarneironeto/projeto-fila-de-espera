package com.arnaldocarneiro.dao;

import com.arnaldocarneiro.model.Especialidade;
import java.util.List;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface EspecialidadeDAO
{
    public Especialidade salvar(Especialidade especialidade) throws Exception;
    public List<Especialidade> salvar(List<Especialidade> especialidades);
    public void excluir(Integer id);
    public Especialidade consultaPorId(Integer id);
    public Especialidade consultaPorNome(String nome);
    public List<Especialidade> consultarTodas();
    public void fechar();
}