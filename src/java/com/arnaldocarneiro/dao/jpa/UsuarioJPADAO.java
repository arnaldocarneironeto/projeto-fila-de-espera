package com.arnaldocarneiro.dao.jpa;

import com.arnaldocarneiro.dao.UsuarioDAO;
import com.arnaldocarneiro.model.Token;
import com.arnaldocarneiro.model.Usuario;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

public class UsuarioJPADAO implements UsuarioDAO
{
    private final EntityManagerFactory factory;
    private final EntityManager manager;

    public UsuarioJPADAO()
    {
        factory = Persistence.createEntityManagerFactory("Projeto_Fila_de_EsperaPU");
        manager = factory.createEntityManager();
    }

    @Override
    public Usuario salvar(Usuario usuario) throws Exception
    {
        manager.getTransaction().begin();
        manager.persist(usuario);
        manager.getTransaction().commit();

        return consultaPorLogin(usuario.getLogin());
    }

    @Override
    public void excluir(Integer id)
    {
        manager.getTransaction().begin();
        manager.remove(consultaPorId(id));
        manager.getTransaction().commit();
    }

    @Override
    public Usuario consultaPorId(Integer id)
    {
        return manager.find(Usuario.class, id);
    }

    @Override
    public Usuario consultaPorLogin(String login)
    {
        Usuario usuario;
        try
        {
            usuario = manager.createNamedQuery("Usuario.findByLogin", Usuario.class).setParameter("login", login).getSingleResult();
        }
        catch (NoResultException e)
        {
            usuario = null;
        }
        return usuario;
    }

    @Override
    public Usuario consultaPorEmail(String email)
    {
        Usuario usuario;
        try
        {
            usuario = manager.createNamedQuery("Usuario.findByEmail", Usuario.class).setParameter("email", email).getSingleResult();
        }
        catch (NoResultException e)
        {
            usuario = null;
        }
        return usuario;
    }

    @Override
    public Usuario consultaPorToken(Token token)
    {
        Usuario usuario;
        try
        {
            usuario = manager.createNamedQuery("Usuario.findByToken", Usuario.class).setParameter("token", token).getSingleResult();
        }
        catch (NoResultException e)
        {
            usuario = null;
        }
        return usuario;
    }

    @Override
    public List<Usuario> consultaTodos()
    {
        return manager.createNamedQuery("Usuario.findAll", Usuario.class).getResultList();
    }

    @Override
    public void fechar()
    {
        if (manager != null)
        {
            manager.close();
        }
        if (factory != null)
        {
            factory.close();
        }
    }
}