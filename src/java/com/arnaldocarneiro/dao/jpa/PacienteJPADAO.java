package com.arnaldocarneiro.dao.jpa;

import com.arnaldocarneiro.dao.PacienteDAO;
import com.arnaldocarneiro.model.Paciente;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PacienteJPADAO implements PacienteDAO
{
    private final EntityManagerFactory factory;
    private final EntityManager manager;

    public PacienteJPADAO()
    {
        factory = Persistence.createEntityManagerFactory("Projeto_Fila_de_EsperaPU");
        manager = factory.createEntityManager();
    }

    @Override
    public Paciente salvar(Paciente paciente) throws Exception
    {
        manager.getTransaction().begin();
        manager.merge(paciente);
        manager.getTransaction().commit();

        return consultaPorLogin(paciente.getLogin());
    }

    @Override
    public List<Paciente> salvar(List<Paciente> pacientes)
    {
        manager.getTransaction().begin();
        for(Paciente paciente: pacientes)
        {
            manager.persist(paciente);
        }
        manager.getTransaction().commit();

        return consultarTodos();
    }

    @Override
    public void excluir(Integer id)
    {
        manager.getTransaction().begin();
        manager.remove(consultaPorId(id));
        manager.getTransaction().commit();
    }

    @Override
    public Paciente consultaPorId(Integer id)
    {
        
        return manager.find(Paciente.class, id);
    }

    @Override
    public Paciente consultaPorLogin(String login)
    {
        return manager.createNamedQuery("Paciente.findByLogin", Paciente.class).setParameter("login", login).getSingleResult();
    }

    @Override
    public List<Paciente> consultarTodos()
    {
        List<Paciente> pacientes;
        try
        {
            pacientes = manager.createNamedQuery("Paciente.findAll", Paciente.class).getResultList();
        }
        catch (Exception e)
        {
            pacientes = new ArrayList<>();
        }

        return pacientes;
    }

    @Override
    public void fechar()
    {
        if (manager != null)
        {
            manager.close();
        }
        if (factory != null)
        {
            factory.close();
        }
    }
}
