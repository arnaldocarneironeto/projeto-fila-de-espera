package com.arnaldocarneiro.dao.jpa;

import com.arnaldocarneiro.dao.FilaDAO;
import com.arnaldocarneiro.model.Fila;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class FilaJPADAO implements FilaDAO
{
    private final EntityManagerFactory factory;
    private final EntityManager manager;

    public FilaJPADAO()
    {
        factory = Persistence.createEntityManagerFactory("Projeto_Fila_de_EsperaPU");
        manager = factory.createEntityManager();
    }

    @Override
    public Fila salvar(Fila fila) throws Exception
    {
        manager.getTransaction().begin();
        manager.persist(fila);
        manager.getTransaction().commit();

        return fila;
    }

    @Override
    public List<Fila> salvar(List<Fila> filas)
    {
        manager.getTransaction().begin();
        for(Fila fila: filas)
        {
            manager.persist(fila);
        }
        manager.getTransaction().commit();

        return consultarTodas();
    }

    @Override
    public void excluir(Integer id)
    {
        manager.getTransaction().begin();
        manager.remove(consultaPorId(id));
        manager.getTransaction().commit();
    }

    @Override
    public Fila consultaPorId(Integer id)
    {
        return manager.find(Fila.class, id);
    }

    @Override
    public List<Fila> consultarTodas()
    {
        List<Fila> filas;
        try
        {
            filas = manager.createNamedQuery("Fila.findAll", Fila.class).getResultList();
        }
        catch (Exception e)
        {
            filas = new ArrayList<>();
        }

        Collections.sort(filas);

        return filas;
    }

    @Override
    public void fechar()
    {
        if (manager != null)
        {
            manager.close();
        }
        if (factory != null)
        {
            factory.close();
        }
    }
}
