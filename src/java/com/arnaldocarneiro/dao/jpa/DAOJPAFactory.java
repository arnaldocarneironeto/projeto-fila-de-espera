package com.arnaldocarneiro.dao.jpa;

import com.arnaldocarneiro.dao.AtendenteDAO;
import com.arnaldocarneiro.dao.DAOFactory;
import com.arnaldocarneiro.dao.EspecialidadeDAO;
import com.arnaldocarneiro.dao.FilaDAO;
import com.arnaldocarneiro.dao.MedicoDAO;
import com.arnaldocarneiro.dao.PacienteDAO;
import com.arnaldocarneiro.dao.TokenDAO;
import com.arnaldocarneiro.dao.UsuarioDAO;

public class DAOJPAFactory implements DAOFactory
{
    @Override
    public AtendenteDAO createAtendenteDAO()
    {
        return new AtendenteJPADAO();
    }

    @Override
    public EspecialidadeDAO createEspecialidadeDAO()
    {
        return new EspecialidadeJPADAO();
    }

    @Override
    public FilaDAO createFilaDAO()
    {
        return new FilaJPADAO();
    }

    @Override
    public MedicoDAO createMedicoDAO()
    {
        return new MedicoJPADAO();
    }

    @Override
    public PacienteDAO createPacienteDAO()
    {
        return new PacienteJPADAO();
    }

    @Override
    public UsuarioDAO createUsuarioDAO()
    {
        return new UsuarioJPADAO();
    }

    @Override
    public TokenDAO createTokenDAO()
    {
        return new TokenJPADAO();
    }
}