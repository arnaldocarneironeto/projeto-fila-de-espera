package com.arnaldocarneiro.dao.jpa;

import com.arnaldocarneiro.dao.EspecialidadeDAO;
import com.arnaldocarneiro.model.Especialidade;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EspecialidadeJPADAO implements EspecialidadeDAO
{
    private final EntityManagerFactory factory;
    private final EntityManager manager;

    public EspecialidadeJPADAO()
    {
        factory = Persistence.createEntityManagerFactory("Projeto_Fila_de_EsperaPU");
        manager = factory.createEntityManager();
    }

    @Override
    public Especialidade salvar(Especialidade especialidade) throws Exception
    {
        manager.getTransaction().begin();
        manager.persist(especialidade);
        manager.getTransaction().commit();

        return consultaPorNome(especialidade.getNome());
    }

    @Override
    public List<Especialidade> salvar(List<Especialidade> especialidades)
    {
        manager.getTransaction().begin();
        for(Especialidade especialidade: especialidades)
        {
            manager.persist(especialidade);
        }
        manager.getTransaction().commit();

        return consultarTodas();
    }

    @Override
    public void excluir(Integer id)
    {
        manager.getTransaction().begin();
        manager.remove(consultaPorId(id));
        manager.getTransaction().commit();
    }

    @Override
    public Especialidade consultaPorId(Integer id)
    {
        return manager.find(Especialidade.class, id);
    }

    @Override
    public Especialidade consultaPorNome(String nome)
    {
        return manager.createNamedQuery("Especialidade.findByNome", Especialidade.class).setParameter("nome", nome).getSingleResult();
    }

    @Override
    public List<Especialidade> consultarTodas()
    {
        List<Especialidade> especialidades;
        try
        {
            especialidades = manager.createNamedQuery("Especialidade.findAll", Especialidade.class).getResultList();
        }
        catch (Exception e)
        {
            especialidades = new ArrayList<>();
        }

        return especialidades;
    }

    @Override
    public void fechar()
    {
        manager.close();
        factory.close();
    }
}