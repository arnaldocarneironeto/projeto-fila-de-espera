package com.arnaldocarneiro.dao.jpa;

import com.arnaldocarneiro.dao.MedicoDAO;
import com.arnaldocarneiro.model.Medico;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

public class MedicoJPADAO implements MedicoDAO
{
    private final EntityManagerFactory factory;
    private final EntityManager manager;

    public MedicoJPADAO()
    {
        factory = Persistence.createEntityManagerFactory("Projeto_Fila_de_EsperaPU");
        manager = factory.createEntityManager();
    }

    @Override
    public Medico salvar(Medico medico) throws Exception
    {
        manager.getTransaction().begin();
        manager.persist(medico);
        manager.getTransaction().commit();

        return consultaPorNome(medico.getNome());
    }

    @Override
    public List<Medico> salvar(List<Medico> medicos)
    {
        manager.getTransaction().begin();
        for(Medico medico: medicos)
        {
            manager.persist(medico);
        }
        manager.getTransaction().commit();

        return consultaTodos();
    }

    @Override
    public void excluir(Integer id)
    {
        manager.getTransaction().begin();
        manager.remove(consultaPorId(id));
        manager.getTransaction().commit();
    }

    @Override
    public Medico consultaPorId(Integer id)
    {
        return manager.find(Medico.class, id);
    }

    @Override
    public Medico consultaPorNome(String nome)
    {
        try
        {
            return manager.createNamedQuery("Medico.findByNome", Medico.class).setParameter("nome", nome).getSingleResult();
        }
        catch(NoResultException e)
        {
            return null;
        }
    }

    @Override
    public List<Medico> consultaTodos()
    {
        List<Medico> medicos;
        try
        {
            medicos = manager.createNamedQuery("Medico.findAll", Medico.class).getResultList();
        }
        catch (NoResultException e)
        {
            medicos = new ArrayList<>();
        }
        return medicos;
    }

    @Override
    public void fechar()
    {
        if (manager != null)
        {
            manager.close();
        }
        if (factory != null)
        {
            factory.close();
        }
    }
}