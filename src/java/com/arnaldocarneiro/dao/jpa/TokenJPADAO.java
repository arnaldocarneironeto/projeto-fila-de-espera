package com.arnaldocarneiro.dao.jpa;

import com.arnaldocarneiro.dao.TokenDAO;
import com.arnaldocarneiro.model.Token;
import com.arnaldocarneiro.model.Usuario;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class TokenJPADAO implements TokenDAO
{
    private final EntityManagerFactory factory;
    private final EntityManager manager;

    public TokenJPADAO()
    {
        factory = Persistence.createEntityManagerFactory("Projeto_Fila_de_EsperaPU");
        manager = factory.createEntityManager();
    }

    @Override
    public Token salvar(Token token) throws Exception
    {
        manager.getTransaction().begin();
        manager.merge(token);
        manager.getTransaction().commit();

        return consultaPorId(token.getId());
    }

    @Override
    public void excluir(Integer id)
    {
        manager.getTransaction().begin();
        manager.remove(consultaPorId(id));
        manager.getTransaction().commit();
    }

    @Override
    public Token consultaPorId(Integer id)
    {
        return manager.find(Token.class, id);
    }

    @Override
    public Token consultaPorUsuario(Usuario usuario)
    {
        try
        {
            return manager.createNamedQuery("Token.findByUsuarioAssociado", Token.class).setParameter("usuarioAssociado", usuario).getSingleResult();
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    @Override
    public Token consultaPorCodigo(String codigo)
    {
        try
        {
            return manager.createNamedQuery("Token.findByCodigo", Token.class).setParameter("codigo", codigo).getSingleResult();
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    @Override
    public void fechar()
    {
        manager.close();
        factory.close();
    }
}