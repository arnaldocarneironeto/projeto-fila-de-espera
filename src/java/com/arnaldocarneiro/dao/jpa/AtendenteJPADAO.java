package com.arnaldocarneiro.dao.jpa;

import com.arnaldocarneiro.dao.AtendenteDAO;
import com.arnaldocarneiro.model.Atendente;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class AtendenteJPADAO implements AtendenteDAO
{
    private final EntityManagerFactory factory;
    private final EntityManager manager;

    public AtendenteJPADAO()
    {
        factory = Persistence.createEntityManagerFactory("Projeto_Fila_de_EsperaPU");
        manager = factory.createEntityManager();
    }

    @Override
    public Atendente salvar(Atendente atendente) throws Exception
    {
        manager.getTransaction().begin();
        manager.merge(atendente);
        manager.getTransaction().commit();

        return consultaPorLogin(atendente.getLogin());
    }

    @Override
    public void excluir(Integer id)
    {
        manager.getTransaction().begin();
        manager.remove(consultaPorId(id));
        manager.getTransaction().commit();
    }

    @Override
    public Atendente consultaPorId(Integer id)
    {
        return manager.find(Atendente.class, id);
    }

    @Override
    public Atendente consultaPorLogin(String login)
    {
        return manager.createNamedQuery("Atendente.findByLogin", Atendente.class).setParameter("login", login).getSingleResult();
    }

    @Override
    public void fechar()
    {
        manager.close();
        factory.close();
    }
}