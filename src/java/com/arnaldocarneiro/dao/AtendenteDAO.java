package com.arnaldocarneiro.dao;

import com.arnaldocarneiro.model.Atendente;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface AtendenteDAO
{
    public Atendente salvar(Atendente atendente) throws Exception;
    public void excluir(Integer id);
    public Atendente consultaPorId(Integer id);
    public Atendente consultaPorLogin(String login);
    public void fechar();
}