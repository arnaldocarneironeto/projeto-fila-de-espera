package com.arnaldocarneiro.dao;

import com.arnaldocarneiro.model.Token;
import com.arnaldocarneiro.model.Usuario;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface TokenDAO
{
    public Token salvar(Token token) throws Exception;
    public void excluir(Integer id);
    public Token consultaPorId(Integer id);
    public Token consultaPorUsuario(Usuario usuario);
    public Token consultaPorCodigo(String codigo);
    public void fechar();
}