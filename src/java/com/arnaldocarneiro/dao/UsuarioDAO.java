package com.arnaldocarneiro.dao;

import com.arnaldocarneiro.model.Token;
import com.arnaldocarneiro.model.Usuario;
import java.util.List;

/**
 * Data Access Object interface to specify how to persist
 * Usuario objects.
 * 
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface UsuarioDAO
{
    /**
     * Persists or updates the Usuario usuario.
     * 
     * @param usuario
     * @return
     * @throws Exception 
     */
    public Usuario salvar(Usuario usuario) throws Exception;
    
    /**
     * 
     * @param id 
     */
    public void excluir(Integer id);
    
    /**
     * 
     * @param id
     * @return 
     */
    public Usuario consultaPorId(Integer id);
    
    /**
     * 
     * @param login
     * @return 
     */
    public Usuario consultaPorLogin(String login);

    /**
     * 
     * @param email
     * @return 
     */
    public Usuario consultaPorEmail(String email);

    /**
     * 
     * @param token
     * @return 
     */
    public Usuario consultaPorToken(Token token);
    
    /**
     * Closes the Data Access Object.
     */
    public void fechar();

    /**
     * @return all users
     */
    public List<Usuario> consultaTodos();
}