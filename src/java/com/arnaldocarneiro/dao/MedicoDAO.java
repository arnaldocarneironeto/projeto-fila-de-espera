package com.arnaldocarneiro.dao;

import com.arnaldocarneiro.model.Medico;
import java.util.List;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface MedicoDAO
{
    public Medico salvar(Medico medico) throws Exception;
    public List<Medico> salvar(List<Medico> medicos);
    public void excluir(Integer id);
    public Medico consultaPorId(Integer id);
    public void fechar();
    public List<Medico> consultaTodos();
    public Medico consultaPorNome(String doctor);
}