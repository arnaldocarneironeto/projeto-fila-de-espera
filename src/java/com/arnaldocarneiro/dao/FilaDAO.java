package com.arnaldocarneiro.dao;

import com.arnaldocarneiro.model.Fila;
import java.util.List;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface FilaDAO
{
    public Fila salvar(Fila fila) throws Exception;
    public List<Fila> salvar(List<Fila> filas);
    public void excluir(Integer id);
    public Fila consultaPorId(Integer id);
    public List<Fila> consultarTodas();
    public void fechar();
}