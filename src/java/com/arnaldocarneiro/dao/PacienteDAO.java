package com.arnaldocarneiro.dao;

import com.arnaldocarneiro.model.Paciente;
import java.util.List;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface PacienteDAO
{
    public Paciente salvar(Paciente paciente) throws Exception;
    public List<Paciente> salvar(List<Paciente> pacientes);
    public void excluir(Integer id);
    public Paciente consultaPorId(Integer id);
    public Paciente consultaPorLogin(String login);
    public List<Paciente> consultarTodos();
    public void fechar();
}