package com.arnaldocarneiro.dao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface DAOFactory
{
    public AtendenteDAO     createAtendenteDAO();
    public EspecialidadeDAO createEspecialidadeDAO();
    public FilaDAO          createFilaDAO();
    public MedicoDAO        createMedicoDAO();
    public PacienteDAO      createPacienteDAO();
    public UsuarioDAO       createUsuarioDAO();
    public TokenDAO         createTokenDAO();
}