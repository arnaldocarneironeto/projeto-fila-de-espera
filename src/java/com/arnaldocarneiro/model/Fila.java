package com.arnaldocarneiro.model;

import com.arnaldocarneiro.utils.DateUtils;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@Entity
@Table(name = "fila", catalog = "waitinglineapplicationdatabase", schema = "")
@XmlRootElement
@NamedQueries(
        {
            @NamedQuery(name = "Fila.findAll", query = "SELECT f FROM Fila f"),
            @NamedQuery(name = "Fila.findById", query = "SELECT f FROM Fila f WHERE f.id = :id"),
            @NamedQuery(name = "Fila.findByDate", query = "SELECT f FROM Fila f WHERE f.dataDaConsulta = :dataDaConsulta"),
            @NamedQuery(name = "Fila.findByEspecialidade", query = "SELECT f FROM Fila f WHERE f.especialidade = :especialidade"),
            @NamedQuery(name = "Fila.findByMedico", query = "SELECT f FROM Fila f WHERE f.medico = :medico")
        })
public class Fila implements Serializable, Comparable<Fila>
{
    private static final long serialVersionUID = 3085197201208582213L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "Data_da_Consulta", nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataDaConsulta;

    @Basic(optional = false)
    @Column(name = "Vagas")
    private Integer vagas;

    @Basic(optional = false)
    @Column(name = "Posicao_Atual")
    private Integer posicaoAtual;

    @OneToOne
    @OrderColumn(name = "Especialidade_Id")
    private Especialidade especialidade;

    @OneToOne
    @OrderColumn(name = "Medico_Id")
    private Medico medico;

    @ManyToMany
    @OrderColumn(name = "Ordem_na_Fila")
    private List<Paciente> pacientes;

    public Fila()
    {
    }

    public Fila(Integer id)
    {
        this.id = id;
    }

    public Fila(Integer id, Date dataDaConsulta, Integer vagas, Integer posicaoAtual, Especialidade especialidade, Medico medico, List<Paciente> pacientes)
    {
        this.id = id;
        this.dataDaConsulta = dataDaConsulta;
        this.vagas = vagas;
        this.posicaoAtual = posicaoAtual;
        this.especialidade = especialidade;
        this.medico = medico;
        this.pacientes = pacientes;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Date getDataDaConsulta()
    {
        return dataDaConsulta;
    }

    public void setDataDaConsulta(Date dataDaConsulta)
    {
        this.dataDaConsulta = dataDaConsulta;
    }

    public Integer getVagas()
    {
        return vagas;
    }

    public void setVagas(Integer vagas)
    {
        this.vagas = vagas;
    }

    public Integer getPosicaoAtual()
    {
        return posicaoAtual;
    }
    
    public void setPosicaoAtual(Integer posicaoAtual)
    {
        this.posicaoAtual = posicaoAtual;
    }

    public Especialidade getEspecialidade()
    {
        return especialidade;
    }

    public void setEspecialidade(Especialidade especialidade)
    {
        this.especialidade = especialidade;
    }

    public Medico getMedico()
    {
        return medico;
    }

    public void setMedico(Medico medico)
    {
        this.medico = medico;
    }

    public List<Paciente> getPacientes()
    {
        return pacientes;
    }

    public void setPacientes(List<Paciente> pacientes)
    {
        this.pacientes = pacientes;
    }
    
    public Integer naEspera()
    {
        return Math.max(getPacientes().size() - this.getVagas(), 0);
    }

    public Boolean isHoje()
    {
        return DateUtils.isToday(this.getDataDaConsulta());
    }

    public Boolean isAntiga()
    {
        return DateUtils.isBeforeDay(this.getDataDaConsulta(), new Date()) || this.getRestantes() <= 0 && this.getPacientes().size() > 0;
    }

    public Integer getRestantes()
    {
        return Math.min(this.getVagas(), this.getPacientes().size()) - this.getPosicaoAtual();
    }

    public Integer getPosicao(Paciente paciente)
    {
        return this.getPacientes().indexOf(paciente) + 1;
    }

    public String getDataStr()
    {
        DateFormat formatter = DateFormat.getDateInstance(DateFormat.MEDIUM, new Locale("pt", "BR"));
        return formatter.format(dataDaConsulta);
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        if (!(object instanceof Fila))
        {
            return false;
        }
        Fila other = (Fila) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return Arrays.toString(this.getPacientes().toArray());
    }

    @Override
    public int compareTo(Fila fila)
    {
        return dataDaConsulta.compareTo(fila.dataDaConsulta);
    }
}