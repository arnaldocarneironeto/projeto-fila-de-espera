package com.arnaldocarneiro.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@Entity
@DiscriminatorValue("A")
@Table(name = "atendente", catalog = "waitinglineapplicationdatabase", schema = "", uniqueConstraints = {@UniqueConstraint(columnNames = {"Matricula"})})
@XmlRootElement
@NamedQueries(
        {
            @NamedQuery(name = "Atendente.findAll", query = "SELECT a FROM Atendente a"),
            @NamedQuery(name = "Atendente.findById", query = "SELECT a FROM Atendente a WHERE a.id = :id"),
            @NamedQuery(name = "Atendente.findByLogin", query = "SELECT a FROM Atendente a WHERE a.login = :login"),
            @NamedQuery(name = "Atendente.findByEmail", query = "SELECT a FROM Atendente a WHERE a.email = :email"),
            @NamedQuery(name = "Atendente.findByMatricula", query = "SELECT a FROM Atendente a WHERE a.matricula = :matricula"),
            @NamedQuery(name = "Atendente.findByNome", query = "SELECT a FROM Atendente a WHERE a.nome = :nome")
        })
public class Atendente extends Usuario
{
    private static final long serialVersionUID = -2286719632300088350L;

    @Basic(optional = false)
    @Column(name = "Matricula", nullable = false, length = 20)
    private String matricula;

    public Atendente()
    {
    }

    public Atendente(Integer id, String login, String senha, String email, String nome, String matricula)
    {
        super(id, login, senha, email, nome);
        this.matricula = matricula;
    }

    @Override
    public String getMatricula()
    {
        return matricula;
    }

    @Override
    public void setMatricula(String matricula)
    {
        this.matricula = matricula;
    }

    @Override
    public String toString()
    {
        return super.toString() + ", matricula=" + matricula;
    }
}