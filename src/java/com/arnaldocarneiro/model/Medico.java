package com.arnaldocarneiro.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@Entity
@Table(name = "medico", catalog = "waitinglineapplicationdatabase", schema = "", uniqueConstraints = {@UniqueConstraint(columnNames = {"Nome"})})
@XmlRootElement
@NamedQueries(
        {
            @NamedQuery(name = "Medico.findAll", query = "SELECT m FROM Medico m"),
            @NamedQuery(name = "Medico.findById", query = "SELECT m FROM Medico m WHERE m.id = :id"),
            @NamedQuery(name = "Medico.findByNome", query = "SELECT m FROM Medico m WHERE m.nome = :nome")
        })
public class Medico implements Serializable
{
    private static final long serialVersionUID = -2488319630102418251L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "Nome", nullable = false, length = 30)
    private String nome;

    @JoinColumn(name = "Especialidade_Id", referencedColumnName = "Id", nullable = false)
    @ManyToOne(optional = false)
    private Especialidade especialidade;

    public Medico()
    {
    }

    public Medico(Integer id)
    {
        this.id = id;
    }

    public Medico(Integer id, String nome)
    {
        this.id = id;
        this.nome = nome;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public Especialidade getEspecialidade()
    {
        return especialidade;
    }

    public void setEspecialidade(Especialidade especialidade)
    {
        this.especialidade = especialidade;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        if (!(object instanceof Medico))
        {
            return false;
        }
        Medico other = (Medico) object;
        if ((this.id == null && other.id != null) ||
            (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "id=" + id + ", nome=" + nome + ", especialidade=" + especialidade;
    }
}