package com.arnaldocarneiro.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@Entity
@Table(name = "token", catalog = "waitinglineapplicationdatabase", schema = "", uniqueConstraints = {@UniqueConstraint(columnNames = {"Codigo"})})
@XmlRootElement
@NamedQueries(
{
    @NamedQuery(name = "Token.findAll", query = "SELECT a FROM Token a"),
    @NamedQuery(name = "Token.findById", query = "SELECT a FROM Token a WHERE a.id = :id"),
    @NamedQuery(name = "Token.findByCodigo", query = "SELECT a FROM Token a WHERE a.codigo = :codigo"),
    @NamedQuery(name = "Token.findByHoraDeCriacao", query = "SELECT a FROM Token a WHERE a.horaDeCriacao = :horaDeCriacao"),
    @NamedQuery(name = "Token.findByUsuarioAssociado", query = "SELECT a FROM Token a WHERE a.usuarioAssociado = :usuarioAssociado")
})
public class Token implements Serializable
{    
    private static final long serialVersionUID = -8454495683594412531L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Integer id;
    
    @Column(name = "Codigo", length = 768, nullable = false, unique = true)
    private String codigo;
    
    @OneToOne
    @JoinColumn(name = "Usuario_Associado_Id")
    private Usuario usuarioAssociado;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "Hora_de_Criacao")
    private Calendar horaDeCriacao;

    public Token()
    {
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public Usuario getUsuarioAssociado()
    {
        return usuarioAssociado;
    }

    public void setUsuarioAssociado(Usuario usuarioAssociado)
    {
        this.usuarioAssociado = usuarioAssociado;
    }

    public Calendar getHoraDeCriacao()
    {
        return horaDeCriacao;
    }

    public void setHoraDeCriacao(Calendar horaDeCriacao)
    {
        this.horaDeCriacao = horaDeCriacao;
    }

    @Override
    public String toString()
    {
        return getId() + "-" + getCodigo() + "-" + getUsuarioAssociado();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Token other = (Token) obj;
        if (!Objects.equals(this.codigo, other.codigo))
        {
            return false;
        }
        if (!Objects.equals(this.id, other.id))
        {
            return false;
        }
        if (!Objects.equals(this.horaDeCriacao, other.horaDeCriacao))
        {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
}