package com.arnaldocarneiro.model;

import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@Entity
@DiscriminatorValue("P")
@Table(name = "paciente", catalog = "waitinglineapplicationdatabase", schema = "")
@XmlRootElement
@NamedQueries(
        {
            @NamedQuery(name = "Paciente.findAll", query = "SELECT p FROM Paciente p"),
            @NamedQuery(name = "Paciente.findById", query = "SELECT p FROM Paciente p WHERE p.id = :id"),
            @NamedQuery(name = "Paciente.findByLogin", query = "SELECT p FROM Paciente p WHERE p.login = :login"),
            @NamedQuery(name = "Paciente.findByEmail", query = "SELECT p FROM Paciente p WHERE p.email = :email"),
            @NamedQuery(name = "Paciente.findByNome", query = "SELECT p FROM Paciente p WHERE p.nome = :nome")
        })
public class Paciente extends Usuario
{
    private static final long serialVersionUID = -2286719632300088350L;

    @ManyToMany(mappedBy = "pacientes")
    @JoinColumn(name = "Fila_Id")
    private List<Fila> filas;

    public Paciente()
    {
    }

    public Paciente(Integer id, String login, String senha, String email, String nome)
    {
        super(id, login, senha, email, nome);
    }

    @Override
    public String getMatricula()
    {
        return null;
    }

    @Override
    public void setMatricula(String matricula)
    {
    }

    public List<Fila> getFilas()
    {
        return filas;
    }

    public void setFilas(List<Fila> filas)
    {
        this.filas = filas;
    }
}