package com.arnaldocarneiro.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@Entity
@Table(name = "especialidade", catalog = "waitinglineapplicationdatabase", schema = "", uniqueConstraints = {@UniqueConstraint(columnNames = {"Nome"})})
@XmlRootElement
@NamedQueries(
        {
            @NamedQuery(name = "Especialidade.findAll", query = "SELECT e FROM Especialidade e"),
            @NamedQuery(name = "Especialidade.findById", query = "SELECT e FROM Especialidade e WHERE e.id = :id"),
            @NamedQuery(name = "Especialidade.findByNome", query = "SELECT e FROM Especialidade e WHERE e.nome = :nome")
        })
public class Especialidade implements Serializable
{
    private static final long serialVersionUID = -887253345627037081L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "Nome", nullable = false, length = 20)
    private String nome;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "especialidade")
    @JoinColumn(name = "Medicos_Ids")
    private List<Medico> listaDeMedicos;

    public Especialidade()
    {
    }

    public Especialidade(Integer id)
    {
        this.id = id;
    }

    public Especialidade(Integer id, String nome)
    {
        this.id = id;
        this.nome = nome;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    @XmlTransient
    public List<Medico> getListaDeMedicos()
    {
        return listaDeMedicos;
    }

    public void setListaDeMedicos(List<Medico> listaDeMedicos)
    {
        this.listaDeMedicos = listaDeMedicos;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        if (!(object instanceof Especialidade))
        {
            return false;
        }
        Especialidade other = (Especialidade) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return nome;
    }
}