package com.arnaldocarneiro.utils;

import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * A utility class for sending e-mail messages
 *
 * @author www.codejava.net
 *
 * Edited by Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class EmailUtility
{
    /**
     * Sends a HTML email message
     * @param host
     * @param port
     * @param userName
     * @param password
     * @param toAddress receivers email address
     * @param subject
     * @param message message encoded in UTF-8 and formated in HTML
     * @return
     * @throws AddressException
     * @throws MessagingException 
     */
    public static boolean sendEmail(String host, String port, final String userName, final String password, String toAddress, String subject, String message) throws AddressException, MessagingException
    {
        try
        {
            Session session = getSession(host, port, userName, password);

            MimeMessage msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress(userName));
            InternetAddress[] toAddresses =
            {
                new InternetAddress(toAddress)
            };
            msg.setRecipients(Message.RecipientType.TO, toAddresses);
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setText(message, "utf-8", "html");

            Transport.send(msg);

            return true;
        }
        catch (MessagingException ex)
        {
            Logger.getLogger(EmailUtility.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private static Session getSession(String host, String port, final String userName, final String password)
    {
        Properties properties = getProperties(host, port);
        Authenticator auth = getAuthenticator(userName, password);
        Session session = Session.getInstance(properties, auth);
        return session;
    }

    private static Authenticator getAuthenticator(final String userName, final String password)
    {
        Authenticator auth = new Authenticator()
        {
            @Override
            public PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(userName, password);
            }
        };
        return auth;
    }

    private static Properties getProperties(String host, String port)
    {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.mime.charset", "UTF-8");
        return properties;
    }
}