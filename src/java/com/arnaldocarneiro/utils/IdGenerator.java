package com.arnaldocarneiro.utils;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class IdGenerator
{
    private static final Random RND = new SecureRandom();
    
    /**
     * número de bits usado pelo SHA512
     */
    private static int numberOfBits = 512;
    
    /**
     * maior base representável com letras e números
     */
    private static int base = 32;
    
    /**
     * número de dígitos no string gerado
     */
    private static int numberOfDigits = numberOfDigits();
    
    public static String generateId(int numberOfBits, int base)
    {
        setBase(base);
        return generateId(numberOfBits);
    }

    public static String generateId(int numberOfBits)
    {
        setNumberOfBits(numberOfBits);
        return generateId();
    }
    
    /**
     * Gera um string aleatório criptograficamente seguro.
     * @return O string gerado.
     */
    public static String generateId()
    {
        String result = new BigInteger(numberOfBits, RND).toString(base);
        result = ensureNumberOfDigits(result);
        return result;        
    }

    /**
     * Certifica-se que o string "number" tenha exatamente "numberOfDigits"
     * dígitos. Caso tenha menos, acrescenta 0's à esquerda até que tenha o
     * numero correto de dígitos. Caso tenha mais, remove os dígitos extras
     * do final.
     * @param number
     * @return 
     */
    private static String ensureNumberOfDigits(String number)
    {
        while(number.length() < numberOfDigits)
        {
            number = "0" + number;
        }
        return number.substring(0, numberOfDigits);
    }

    /**
     * Calcula o número mínimo de dígitos necessários para representar um número
     * com "numberOfBits" bits na base "base".
     * @return 
     */
    private static int numberOfDigits()
    {
        return (int) Math.ceil(numberOfBits / (Math.log(base) / Math.log(2)));
    }

    private static void setNumberOfBits(int numberOfBits)
    {
        IdGenerator.numberOfBits = numberOfBits;
        IdGenerator.numberOfDigits = numberOfDigits();
    }

    private static void setBase(int base)
    {
        IdGenerator.base = base;
        IdGenerator.numberOfDigits = numberOfDigits();
    }
}