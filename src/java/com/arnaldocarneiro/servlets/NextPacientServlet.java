package com.arnaldocarneiro.servlets;

import com.arnaldocarneiro.dao.DAOFactory;
import com.arnaldocarneiro.dao.FilaDAO;
import com.arnaldocarneiro.dao.jpa.DAOJPAFactory;
import com.arnaldocarneiro.model.Fila;
import com.arnaldocarneiro.model.Paciente;
import com.arnaldocarneiro.model.Usuario;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class NextPacientServlet extends HttpServlet
{
    private static final long serialVersionUID = -8162635248124053650L;

    private static final Object LOCK = new Object();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        String matricula = null;
        try
        {
            matricula = ((Usuario) session.getAttribute("user")).getMatricula();
        }
        catch(Exception e)
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
        if(matricula != null)
        {
            DAOFactory factory = new DAOJPAFactory();
            FilaDAO filaDAO = factory.createFilaDAO();

            Integer id = Integer.parseInt(request.getParameter("q"));
            RequestDispatcher rd;

            synchronized(LOCK)
            {
                Fila fila = filaDAO.consultaPorId(id);

                int position = fila.getPosicaoAtual();
                request.setAttribute("q", id);

                if (position < fila.getVagas() && position < fila.getPacientes().size())
                {
                    request.setAttribute("paciente", fila.getPacientes().get(position++));
                    request.setAttribute("r", fila.getRestantes() - 1);

                    fila.setPosicaoAtual(position);

                    try
                    {
                        filaDAO.salvar(fila);
                    }
                    catch (Exception ex)
                    {
                        Logger.getLogger(NextPacientServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else
                {
                    Paciente ninguem = new Paciente();
                    ninguem.setNome("Fila vazia");
                    request.setAttribute("paciente", ninguem);
                    request.setAttribute("r", 0);
                }
            }

            filaDAO.fechar();

            rd = request.getRequestDispatcher("/next.jsp");
            rd.forward(request, response);
        }
        else
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}