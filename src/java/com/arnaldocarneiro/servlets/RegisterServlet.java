package com.arnaldocarneiro.servlets;

import com.arnaldocarneiro.dao.DAOFactory;
import com.arnaldocarneiro.dao.PacienteDAO;
import com.arnaldocarneiro.dao.jpa.DAOJPAFactory;
import com.arnaldocarneiro.model.Paciente;
import com.arnaldocarneiro.model.Usuario;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class RegisterServlet extends HttpServlet
{
    private static final long serialVersionUID = -1926848320018379310L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        Usuario usuario = null;
        try
        {
            usuario = (Usuario) session.getAttribute("user");
        }
        catch(Exception e)
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
        if(usuario == null)
        {
            DAOFactory factory = new DAOJPAFactory();
            PacienteDAO pacienteDAO = factory.createPacienteDAO();

            String nome = request.getParameter("name");
            String login = request.getParameter("login");
            String password = request.getParameter("password");
            String passconf = request.getParameter("password_confirmation");
            String email = request.getParameter("email");

            String responseMessage = "Problema no cadastro do usuário.";
            boolean passwordsMatch = password == null ? passconf == null : password.equals(passconf);
            if (passwordsMatch)
            {
                Paciente paciente = new Paciente();
                paciente.setLogin(login);
                paciente.setSenha(password);
                paciente.setEmail(email);
                paciente.setNome(nome);

                try
                {
                    pacienteDAO.salvar(paciente);

                    responseMessage = "Usuário cadastrado com sucesso.";
                }
                catch (Exception e)
                {
                    responseMessage += " Nome de usuário ou e-mail já existe no sistema";
                    throw new ServletException(responseMessage);
                }
            }
            else
            {
                responseMessage += " Senhas não conferem.";
                throw new ServletException(responseMessage);
            }

            pacienteDAO.fechar();

            attachAttributesToSession(request, responseMessage);

            RequestDispatcher rd = request.getRequestDispatcher("/message.jsp");
            rd.forward(request, response);
        }
        else
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
    }

    private void attachAttributesToSession(HttpServletRequest request, String responseMessage)
    {
        HttpSession session = request.getSession();
        session.setAttribute("msg", responseMessage);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }
    // </editor-fold>
}