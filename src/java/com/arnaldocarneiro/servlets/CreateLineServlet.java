package com.arnaldocarneiro.servlets;

import com.arnaldocarneiro.dao.DAOFactory;
import com.arnaldocarneiro.dao.FilaDAO;
import com.arnaldocarneiro.dao.MedicoDAO;
import com.arnaldocarneiro.dao.jpa.DAOJPAFactory;
import com.arnaldocarneiro.model.Fila;
import com.arnaldocarneiro.model.Medico;
import com.arnaldocarneiro.model.Usuario;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class CreateLineServlet extends HttpServlet
{
    private static final long serialVersionUID = 3815943223649396215L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws javax.servlet.ServletException
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        String matricula = null;
        try
        {
            matricula = ((Usuario) session.getAttribute("user")).getMatricula();
        }
        catch(Exception e)
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
        if(matricula != null)
        {
            DAOFactory factory = new DAOJPAFactory();
            FilaDAO filaDAO = factory.createFilaDAO();
            MedicoDAO medicoDAO = factory.createMedicoDAO();

            String doctor = request.getParameter("doctor");
            String spots = request.getParameter("spots");
            String date = request.getParameter("date");

            Medico medico = medicoDAO.consultaPorNome(doctor);
            String resultMessage = createLine(filaDAO, spots, date, medico);
            if (!"Fila criada com sucesso.".equals(resultMessage))
            {
                throw new ServletException(resultMessage);
            }

            filaDAO.fechar();
            medicoDAO.fechar();

            attachAttributesToSession(request, resultMessage);

            RequestDispatcher rd = request.getRequestDispatcher("/message.jsp");
            rd.forward(request, response);
        }
        else
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
    }

    private synchronized String createLine(FilaDAO filaDAO, String spots, String date, Medico medico)
    {
        String resultMessage;
        try
        {
            int vagas = Integer.parseInt(spots);
            if (vagas > 28)
            {
                throw new IllegalArgumentException("Número de vagas inválido: " + spots);
            }
            
            int day = getPartOfDate(date, 0);
            int month = getPartOfDate(date, 1);
            int year = getPartOfDate(date, 2);
            Date data = new GregorianCalendar(year, month - 1, day).getTime();
            
            if (medico == null)
            {
                throw new IllegalArgumentException("Médico não encontrado.");
            }

            Fila fila = new Fila();
            fila.setDataDaConsulta(data);
            fila.setEspecialidade(medico.getEspecialidade());
            fila.setMedico(medico);
            fila.setVagas(vagas);
            fila.setPosicaoAtual(0);
            fila.setPacientes(new ArrayList<>());

            try
            {
                filaDAO.salvar(fila);
                resultMessage = "Fila criada com sucesso.";
            }
            catch(Exception e)
            {
                resultMessage = "Erro de persistencia";
            }
        }
        catch (NumberFormatException ex)
        {
            resultMessage = "Número de vagas inválido: " + spots;
        }
        catch (IllegalArgumentException ex)
        {
            resultMessage = ex.getLocalizedMessage();
        }
        catch (ArrayIndexOutOfBoundsException ex)
        {
            resultMessage = "Data inválida: " + date;
        }
        return resultMessage;
    }

    private int getPartOfDate(String date, int position) throws NumberFormatException
    {
        return Integer.parseInt((date.split("/"))[position]);
    }

    private void attachAttributesToSession(HttpServletRequest request, String resultMessage)
    {
        HttpSession session = request.getSession();
        session.setAttribute("msg", resultMessage);
        session.removeAttribute("medicos");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}