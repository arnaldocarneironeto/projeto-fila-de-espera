package com.arnaldocarneiro.servlets;

import com.arnaldocarneiro.dao.DAOFactory;
import com.arnaldocarneiro.dao.TokenDAO;
import com.arnaldocarneiro.dao.UsuarioDAO;
import com.arnaldocarneiro.dao.jpa.DAOJPAFactory;
import com.arnaldocarneiro.model.Token;
import com.arnaldocarneiro.model.Usuario;
import java.io.IOException;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class ChangePassServlet extends HttpServlet
{
    private static final long serialVersionUID = 8585638572706576823L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        Usuario usuario = null;
        String codigo = null;
        try
        {
            usuario = (Usuario) session.getAttribute("user");
            codigo = request.getParameter("t");
        }
        catch (Exception e)
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
        if (usuario == null && codigo != null)
        {
            DAOFactory factory = new DAOJPAFactory();
            TokenDAO tokenDAO = factory.createTokenDAO();

            Token token = tokenDAO.consultaPorCodigo(codigo);

            if(token != null)
            {
                long totalTimeInMinutes = getElapsedTime(token);
                if(totalTimeInMinutes < 15)
                {
                    UsuarioDAO usuarioDAO = factory.createUsuarioDAO();
                    usuario = usuarioDAO.consultaPorToken(token);

                    session.setAttribute("token", token);
                    session.setAttribute("usuario", usuario);

                    request.getRequestDispatcher("/change_pass.jsp").forward(request, response);
                }
                else
                {
                    throw new ServletException("Mais de 15 minutos se passaram desde que você solicitou a recuperação de senha. Faça a solicitação novamente.");
                }
            }
            else
            {
                throw new ServletException("Não há registro desta solicitação de recuperação de senha no sistema. Usuário não autorizado a acessar esta funcionalidade.");
            }

            tokenDAO.fechar();
        }
        else
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
    }

    private long getElapsedTime(Token token)
    {
        Calendar now = Calendar.getInstance();
        long totalTimeIntMilis = now.getTimeInMillis() - token.getHoraDeCriacao().getTimeInMillis();
        long totalTimeInMinutes = totalTimeIntMilis / (60 * 1000);
        return totalTimeInMinutes;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }
    // </editor-fold>
}