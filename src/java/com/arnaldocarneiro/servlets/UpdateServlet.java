package com.arnaldocarneiro.servlets;

import com.arnaldocarneiro.dao.AtendenteDAO;
import com.arnaldocarneiro.dao.DAOFactory;
import com.arnaldocarneiro.dao.PacienteDAO;
import com.arnaldocarneiro.dao.jpa.DAOJPAFactory;
import com.arnaldocarneiro.model.Atendente;
import com.arnaldocarneiro.model.Paciente;
import com.arnaldocarneiro.model.Usuario;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class UpdateServlet extends HttpServlet
{
    private static final long serialVersionUID = 4239893126537420450L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        Usuario usuario = null;
        try
        {
            usuario = (Usuario) session.getAttribute("user");
        }
        catch(Exception e)
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
        if(usuario != null)
        {
            String login = request.getParameter("login");

            if(usuario.getLogin() == null? login != null: !usuario.getLogin().equals(login))
            {
                throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
            }

            String nome = request.getParameter("name");
            String password = request.getParameter("password");
            String newpass = request.getParameter("new_password");
            String newpassconf = request.getParameter("password_confirmation");
            String email = request.getParameter("email");

            if((password == null? usuario.getSenha() == null: password.equals(usuario.getSenha())) && (newpass == null? newpassconf == null: newpass.equals(newpassconf)))
            {
                if("".equals(nome) == false)
                {
                    usuario.setNome(nome);
                }
                if("".equals(newpass) == false)
                {
                    usuario.setSenha(newpass);
                }
                if("".equals(email) == false)
                {
                    usuario.setEmail(email);
                }
                DAOFactory factory = new DAOJPAFactory();
                if(usuario.getMatricula() != null)
                {
                    AtendenteDAO atendenteDAO = factory.createAtendenteDAO();
                    try
                    {
                        atendenteDAO.salvar((Atendente) usuario);
                        session.setAttribute("msg", "Seus dados foram atualizados com sucesso.");
                    }
                    catch (Exception ex)
                    {
                        throw new ServletException("Ocorreu um erro ao atualizar seus dados.");
                    }
                    atendenteDAO.fechar();
                }
                else
                {
                    PacienteDAO pacienteDAO = factory.createPacienteDAO();
                    try
                    {
                        pacienteDAO.salvar((Paciente) usuario);
                        session.setAttribute("msg", "Seus dados foram atualizados com sucesso.");
                    }
                    catch (Exception ex)
                    {
                        throw new ServletException("Ocorreu um erro ao atualizar seus dados.");
                    }
                    pacienteDAO.fechar();
                }
            }
            else
            {
                throw new ServletException("Senha inválida ou senhas não conferem.");
            }
            request.getRequestDispatcher("/message.jsp").forward(request, response);
        }
        else
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }
    // </editor-fold>
}