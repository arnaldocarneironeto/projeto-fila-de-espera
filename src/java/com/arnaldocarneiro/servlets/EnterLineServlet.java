package com.arnaldocarneiro.servlets;

import com.arnaldocarneiro.dao.DAOFactory;
import com.arnaldocarneiro.dao.FilaDAO;
import com.arnaldocarneiro.dao.jpa.DAOJPAFactory;
import com.arnaldocarneiro.model.Fila;
import com.arnaldocarneiro.model.Paciente;
import com.arnaldocarneiro.model.Usuario;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class EnterLineServlet extends HttpServlet
{
    private static final long serialVersionUID = -4845591223413641289L;

    private static final Object LOCK = new Object();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        String matricula = null;
        try
        {
            matricula = ((Usuario) session.getAttribute("user")).getMatricula();
        }
        catch(Exception e)
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
        if(matricula == null)
        {
            Integer lineId = Integer.parseInt(request.getPathInfo().split("/")[1]);

            DAOFactory factory = new DAOJPAFactory();
            FilaDAO filaDAO = factory.createFilaDAO();

            try
            {
                Paciente paciente = (Paciente) request.getSession().getAttribute("user");

                synchronized(LOCK)
                {
                    Fila fila = filaDAO.consultaPorId(lineId);

                    List<Paciente> pacientes = fila.getPacientes();

                    if (pacientes.contains(paciente))
                    {
                        fila.getPacientes().remove(paciente);
                    }
                    else
                    {
                        fila.getPacientes().add(paciente);
                    }

                    try
                    {
                        filaDAO.salvar(fila);
                    }
                    catch (Exception ex)
                    {
                        throw new ServletException("Não foi possível salvar a fila");
                    }
                }
            }
            catch (ClassCastException | ArrayIndexOutOfBoundsException e)
            {
                throw new ServletException("Um erro ocorreu. Tente novamente com outros valores.");
            }
            catch (NumberFormatException e)
            {
                throw new ServletException("Número inválido: " + e);
            }

            filaDAO.fechar();

            response.sendRedirect(request.getContextPath());
        }
        else
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }
    // </editor-fold>
}