package com.arnaldocarneiro.servlets;

import com.arnaldocarneiro.dao.AtendenteDAO;
import com.arnaldocarneiro.dao.DAOFactory;
import com.arnaldocarneiro.dao.PacienteDAO;
import com.arnaldocarneiro.dao.jpa.DAOJPAFactory;
import com.arnaldocarneiro.model.Atendente;
import com.arnaldocarneiro.model.Paciente;
import com.arnaldocarneiro.model.Usuario;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class ManageUserServlet extends HttpServlet
{
    private static final long serialVersionUID = -1858826417029437419L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        String matricula = null;
        try
        {
            matricula = ((Usuario) session.getAttribute("user")).getMatricula();
        }
        catch(Exception e)
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
        if(matricula != null)
        {
            Usuario usuario = (Usuario) session.getAttribute("usuario");
            matricula = request.getParameter("matricula");
            String usertype = request.getParameter("usertype");

            if(usuario != null)
            {
                DAOFactory factory = new DAOJPAFactory();
                PacienteDAO pacienteDAO = factory.createPacienteDAO();
                AtendenteDAO atendenteDAO = factory.createAtendenteDAO();

                if(usertype != null && "atendente".equals(usertype))
                {
                    if(matricula != null && "".equals(matricula) == false)
                    {
                        Atendente atendente = new Atendente(usuario.getId(), usuario.getLogin(), usuario.getSenha(), usuario.getEmail(), usuario.getNome(), matricula);
                        try
                        {
                            pacienteDAO.excluir(usuario.getId());
                        }
                        catch (Exception ex)
                        {
                            throw new ServletException("Houve um problema ao excliur o usuário.");
                        }
                        try
                        {
                            atendenteDAO.salvar(atendente);
                        }
                        catch (Exception ex)
                        {
                            throw new ServletException("Houve um problema ao salvar o usuário.");
                        }
                    }
                    else
                    {
                        throw new ServletException("A matrícula é obrigatoria para atendentes.");
                    }
                }
                else
                {
                    Paciente paciente = new Paciente(usuario.getId(), usuario.getLogin(), usuario.getSenha(), usuario.getEmail(), usuario.getNome());
                    atendenteDAO.excluir(usuario.getId());
                    try
                    {
                        pacienteDAO.salvar(paciente);
                    }
                    catch (Exception ex)
                    {
                        Logger.getLogger(ManageUserServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                atendenteDAO.fechar();
                pacienteDAO.fechar();
            }
            else
            {
                throw new ServletException("Dados inválidos.");
            }
            request.getRequestDispatcher("ManageUsers").forward(request, response);
        }
        else
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }
    // </editor-fold>
}