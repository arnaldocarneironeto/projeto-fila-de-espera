package com.arnaldocarneiro.servlets;

import com.arnaldocarneiro.dao.DAOFactory;
import com.arnaldocarneiro.dao.MedicoDAO;
import com.arnaldocarneiro.dao.UsuarioDAO;
import com.arnaldocarneiro.dao.jpa.DAOJPAFactory;
import com.arnaldocarneiro.model.Medico;
import com.arnaldocarneiro.model.Usuario;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class LoginServlet extends HttpServlet
{
    private static final long serialVersionUID = -534825097032223525L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        Usuario usuario = null;
        try
        {
            usuario = (Usuario) session.getAttribute("user");
        }
        catch(Exception e)
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
        if(usuario == null)
        {
            DAOFactory factory = new DAOJPAFactory();
            UsuarioDAO usuarioDAO = factory.createUsuarioDAO();
            MedicoDAO medicoDAO = factory.createMedicoDAO();

            String login = request.getParameter("login");
            String senha = request.getParameter("password");

            usuario = usuarioDAO.consultaPorLogin(login);
            List<Medico> medicos = medicoDAO.consultaTodos();

            attachAttributesToSession(request, usuario, senha, medicos);

            usuarioDAO.fechar();
            medicoDAO.fechar();

            RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
            rd.forward(request, response);
        }
        else
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
    }

    private void attachAttributesToSession(HttpServletRequest request, Usuario usuario, String senha, List<Medico> medicos)
    {
        HttpSession session = request.getSession();
        if (usuario != null && usuario.getSenha().equals(senha))
        {
            session.setAttribute("user", usuario);
        }
        else
        {
            session.removeAttribute("user");
        }
        session.setAttribute("medicos", medicos);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }
    // </editor-fold>
}
