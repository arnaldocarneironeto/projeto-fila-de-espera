package com.arnaldocarneiro.servlets;

import com.arnaldocarneiro.dao.DAOFactory;
import com.arnaldocarneiro.dao.TokenDAO;
import com.arnaldocarneiro.dao.UsuarioDAO;
import com.arnaldocarneiro.dao.jpa.DAOJPAFactory;
import com.arnaldocarneiro.model.Token;
import com.arnaldocarneiro.model.Usuario;
import static com.arnaldocarneiro.utils.EmailUtility.sendEmail;
import com.arnaldocarneiro.utils.IdGenerator;
import java.io.IOException;
import java.util.Calendar;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class PassRecoverServlet extends HttpServlet
{
    private static final long serialVersionUID = 2630284473007969399L;
    private String host;
    private String port;
    private String user;
    private String pass;

    @Override
    public void init() throws ServletException
    {
        ServletContext context = getServletContext();
        host = context.getInitParameter("host");
        port = context.getInitParameter("port");
        user = context.getInitParameter("user");
        pass = context.getInitParameter("pass");
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        Usuario usuario = null;
        String login = null;
        String email = null;
        try
        {
            usuario = (Usuario) session.getAttribute("user");
            login = request.getParameter("login");
            email = request.getParameter("email");
        }
        catch (Exception e)
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
        if (usuario == null && login != null && email != null)
        {
            DAOFactory factory = new DAOJPAFactory();

            usuario = getUsuario(factory, login, email);
            
            if(usuario != null)
            {
                Token token = generateNewToken(factory, usuario);

                try
                {
                    final String subject = "Recuperação de acesso ao \"Projeto Fila de Espera\"";
                    final String message = "Você (ou alguém com acesso ao seu nome" +
                                           " de usuário ou ao seu endereço de email) " +
                                           "solicitou a recuperação de acesso a este " +
                                           "serviço. Clique no <em>link</em> abaixo para alterar " +
                                           "a sua senha (copie-o e cole-o na barra de " +
                                           "endereços, caso necessário) ou <b>ignore</b> " +
                                           "este email caso não tenha solicitado a " +
                                           "alteração da sua senha.";
                    String link = request.getServerName() + ":" + request.getServerPort() +
                                  request.getContextPath() + "/ChangePass?t=" + token.getCodigo();
                    final String html_message = "<html><body><p>" + message + "</p><a href=\"" +
                                                link + "\">" + link + "</a></body></html>";
                    sendEmail(host, port, user, pass, usuario.getEmail(), subject, html_message);
                }
                catch (Exception ex)
                {
                    throw new ServletException("Erro ao enviar o email:" + ex + ".");
                }
                finally
                {
                    showSuccessMessage(session, request, response);
                }
            }
            else
            {
                showSuccessMessage(session, request, response);
            }
        }
        else
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
    }

    private void showSuccessMessage(HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        session.setAttribute("msg", "Um email com as informações de recuperação foi enviado.");
        RequestDispatcher rd = request.getRequestDispatcher("/message.jsp");
        rd.forward(request, response);
    }

    private Usuario getUsuario(DAOFactory factory, String login, String email) throws ServletException
    {
        Usuario usuario;
        UsuarioDAO usuarioDAO = factory.createUsuarioDAO();
        if("".equals(login) == false)
        {
            usuario = usuarioDAO.consultaPorLogin(login);
        }
        else if("".equals(email) == false)
        {
            usuario = usuarioDAO.consultaPorEmail(email);
        }
        else
        {
            throw new ServletException("É preciso informar o nome ou o email.");
        }
        usuarioDAO.fechar();
        return usuario;
    }

    private Token generateNewToken(DAOFactory factory, Usuario usuario) throws ServletException
    {
        TokenDAO tokenDAO = factory.createTokenDAO();
        Token token = tokenDAO.consultaPorUsuario(usuario);
        if (token != null)
        {
            tokenDAO.excluir(token.getId());
        }
        token = new Token();
        token.setCodigo(IdGenerator.generateId());
        token.setUsuarioAssociado(usuario);
        token.setHoraDeCriacao(Calendar.getInstance());
        token.setId(usuario.getId());
        try
        {
            tokenDAO.salvar(token);
        }
        catch (Exception ex)
        {
            throw new ServletException("Erro na requisição de nova senha.");
        }
        tokenDAO.fechar();
        return token;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }
    // </editor-fold>
}