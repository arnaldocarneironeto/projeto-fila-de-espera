package com.arnaldocarneiro.servlets;

import com.arnaldocarneiro.dao.AtendenteDAO;
import com.arnaldocarneiro.dao.DAOFactory;
import com.arnaldocarneiro.dao.EspecialidadeDAO;
import com.arnaldocarneiro.dao.FilaDAO;
import com.arnaldocarneiro.dao.MedicoDAO;
import com.arnaldocarneiro.dao.PacienteDAO;
import com.arnaldocarneiro.dao.UsuarioDAO;
import com.arnaldocarneiro.dao.jpa.DAOJPAFactory;
import com.arnaldocarneiro.model.Atendente;
import com.arnaldocarneiro.model.Especialidade;
import com.arnaldocarneiro.model.Fila;
import com.arnaldocarneiro.model.Medico;
import com.arnaldocarneiro.model.Paciente;
import com.arnaldocarneiro.model.Usuario;
import com.arnaldocarneiro.utils.Triple;
import com.arnaldocarneiro.utils.WebServiceUtils;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class MainServlet extends HttpServlet
{
    private static final long serialVersionUID = 1244623616926686748L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
        rd.forward(request, response);
    }

    private void attachAttributesToSession(HttpServletRequest request, String amanhaString, List<Fila> filas)
    {
        HttpSession session = request.getSession();
        session.setAttribute("amanha", amanhaString);
        session.setAttribute("filas", filas);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }
    // </editor-fold>

    //Cria o usuário admin com a senha padrão, caso ele ainda não exista no banco
    @Override
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        boolean debug = "true".equalsIgnoreCase(config.getInitParameter("debug"));
        boolean gender_debug = "true".equalsIgnoreCase(config.getInitParameter("gender-debug"));

        DAOFactory factory = new DAOJPAFactory();
        UsuarioDAO usuarioDAO = factory.createUsuarioDAO();
        AtendenteDAO atendenteDAO = factory.createAtendenteDAO();

        Usuario usuario = usuarioDAO.consultaPorLogin("admin");

        if(usuario == null)
        {
            Atendente admin = new Atendente();
            admin.setEmail("admin@localhost");
            admin.setLogin("admin");
            admin.setMatricula("adm001");
            admin.setNome("Admin");
            admin.setSenha("admin");

            try
            {
                atendenteDAO.salvar(admin);
            }
            catch (Exception ex)
            {
                Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            if(debug)
            {
                debugPopulateDatabase(factory, gender_debug);
            }
        }

        usuarioDAO.fechar();
        atendenteDAO.fechar();
    }

    static
    {
    }

    // Usuários gerados aleatoriamente (http://www.fakenamegenerator.com/gen-random-br-br.php) para testes
    // Caso a rede esteja indisponível, os usuário são escolhidos de uma lista aleatória gerada previamente
    private static void debugPopulateDatabase(DAOFactory factory, boolean gender_debug)
    {
        Random rnd = new SecureRandom();

        List<Paciente> pacientes = createPacienteList(factory);
        
        List<Especialidade> especialidades = createEspecialidadeList(factory);
        
        List<Medico> medicos = createMedicoList(especialidades, rnd, gender_debug, factory);
        
        createFilaList(medicos, rnd, pacientes, factory);
    }

    private static List<Fila> createFilaList(List<Medico> medicos, Random rnd, List<Paciente> pacientes, DAOFactory factory)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -3);

        List<Fila> filas = new ArrayList<>();
        int proximoMedico = 0;
        for(int i = 0, flip = 0; i < 40; ++i, ++flip)
        {
            Medico medico = medicos.get(proximoMedico);
            proximoMedico = (++proximoMedico) % medicos.size();
            int numeroDePacientesNestaFila = rnd.nextInt(7) + 1;

            Fila fila = new Fila();
            fila.setDataDaConsulta(calendar.getTime());
            fila.setMedico(medico);
            fila.setEspecialidade(medico.getEspecialidade());
            fila.setPosicaoAtual(0);
            fila.setVagas(rnd.nextInt(7) + 1);

            List<Paciente> pacientesNestaFila = new ArrayList<>();
            for(int j = 0; j < numeroDePacientesNestaFila; ++j)
            {
                int k;
                do
                {
                    k = rnd.nextInt(pacientes.size());
                }
                while(pacientesNestaFila.contains(pacientes.get(k)));
                pacientesNestaFila.add(pacientes.get(k));
            }
            fila.setPacientes(pacientesNestaFila);

            filas.add(fila);
            if(flip % 3 == 2)
            {
                calendar.add(Calendar.DATE, 1);
            }
        }

        FilaDAO filaDAO = factory.createFilaDAO();
        filas = filaDAO.salvar(filas);
        filaDAO.fechar();

        return filas;
    }

    private static List<Medico> createMedicoList(List<Especialidade> especialidades, Random rnd, boolean gender_debug, DAOFactory factory)
    {
        List<String> medicoNames = new ArrayList<>();
        for(int i = 0; i < 6; ++i)
        {
            final Triple<String, String, String> tripleNameEmailGender = WebServiceUtils.getRandomTripleNameEmailGender();
            String nome = tripleNameEmailGender.getLeft();
            if("".equals(nome) == false)
            {
                if (gender_debug)
                {
                    nome = ("f".equals(tripleNameEmailGender.getRight())? "Dra. ": "Dr. ") + nome;
                }
                medicoNames.add(nome);
            }
        }
        if(medicoNames.size() < 6)
        {
            if (gender_debug)
            {
                medicoNames = Arrays.asList("Dr. Felipe Souza Ribeiro", "Dr. Murilo Dias Barbosa", "Dr. Gabriel Martins Barros", "Dra. Carolina Ribeiro Almeida", "Dra. Clara Costa Lima", "Dra. Aline Castro Rocha");
            }
            else
            {
                medicoNames = Arrays.asList("Felipe Souza Ribeiro", "Murilo Dias Barbosa", "Gabriel Martins Barros", "Carolina Ribeiro Almeida", "Clara Costa Lima", "Aline Castro Rocha");
            }
        }

        List<Medico> medicos = new ArrayList<>();

        for(String nome: medicoNames)
        {
            Medico m = new Medico();
            m.setNome(nome);
            m.setEspecialidade(especialidades.get(rnd.nextInt(especialidades.size())));
            medicos.add(m);
        }

        MedicoDAO medicoDAO = factory.createMedicoDAO();
        medicos = medicoDAO.salvar(medicos);
        medicoDAO.fechar();

        return medicos;
    }

    private static List<Especialidade> createEspecialidadeList(DAOFactory factory)
    {
        List<String> especialidadeNames = Arrays.asList("Virose", "Fastio", "Pantim", "Pereba");
        List<Especialidade> especialidades = new ArrayList<>();

        for(String doenca: especialidadeNames)
        {
            Especialidade e = new Especialidade();
            e.setNome(doenca);
            especialidades.add(e);
        }

        EspecialidadeDAO especialidadeDAO = factory.createEspecialidadeDAO();
        especialidades = especialidadeDAO.salvar(especialidades);
        especialidadeDAO.fechar();

        return especialidades;
    }

    private static List<Paciente> createPacienteList(DAOFactory factory)
    {
        List<String> pacienteNames = new ArrayList<>();
        List<String> pacienteEmails = new ArrayList<>();
        for(int i = 0; i < 9; ++i)
        {
            Triple<String, String, String> triplaNomeEmailGenero = WebServiceUtils.getRandomTripleNameEmailGender();
            if(triplaNomeEmailGenero.getLeft().equals("") == false)
            {
                pacienteNames.add(triplaNomeEmailGenero.getLeft());
                pacienteEmails.add(triplaNomeEmailGenero.getMiddle());
            }
        }
        if(pacienteNames.size() != 9)
        {
            pacienteNames = Arrays.asList("Gabrielly Almeida Fernandes", "Julian Fernandes Melo", "Laura Pinto Gomes", "Ana Silva Martins", "Giovanna Costa Almeida", "Danilo Pereira Araujo", "Miguel Souza Correia", "Breno Araujo Melo", "Pedro Pereira Carvalho");
            pacienteEmails = Arrays.asList("GabriellyAlmeidaFernandes@rhyta.com", "JulianFernandesMelo@dayrep.com", "LauraPintoGomes@jourrapide.com", "AnaSilvaMartins@teleworm.us", "GiovannaCostaAlmeida@teleworm.us", "DaniloPereiraAraujo@jourrapide.com", "MiguelSouzaCorreia@armyspy.com", "BrenoAraujoMelo@dayrep.com", "PedroPereiraCarvalho@rhyta.com");
        }

        List<Paciente> pacientes = new ArrayList<>();
        for(int i = 0; i < pacienteNames.size(); ++i)
        {
            Paciente p = new Paciente();
            p.setNome(pacienteNames.get(i));
            p.setEmail(pacienteEmails.get(i));
            p.setLogin("paciente" + (i + 1));
            p.setSenha("senha" + (i + 1));
            pacientes.add(p);
        }

        PacienteDAO pacienteDAO = factory.createPacienteDAO();
        pacientes = pacienteDAO.salvar(pacientes);
        pacienteDAO.fechar();

        return pacientes;
    }
}