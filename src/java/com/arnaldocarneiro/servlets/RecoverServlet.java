package com.arnaldocarneiro.servlets;

import com.arnaldocarneiro.dao.DAOFactory;
import com.arnaldocarneiro.dao.TokenDAO;
import com.arnaldocarneiro.dao.UsuarioDAO;
import com.arnaldocarneiro.dao.jpa.DAOJPAFactory;
import com.arnaldocarneiro.model.Token;
import com.arnaldocarneiro.model.Usuario;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class RecoverServlet extends HttpServlet
{
    private static final long serialVersionUID = 6207189534724025836L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        Usuario usuario = null;
        try
        {
            usuario = (Usuario) session.getAttribute("user");
        }
        catch (Exception e)
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
        if (usuario == null)
        {
            String[] nullTokenUser = {"-1", "-1"};
            String[] tokenUser = request.getParameter("g") != null? request.getParameter("g").split("[$]", 2): nullTokenUser;
            Integer userId = Integer.parseInt(tokenUser[0]);
            String tokenCod = tokenUser[1];
            if(userId != -1)
            {
                DAOFactory factory = new DAOJPAFactory();
                TokenDAO tokenDAO = factory.createTokenDAO();
                Token token = tokenDAO.consultaPorCodigo(tokenCod);

                if(token != null)
                {
                    String password = request.getParameter("password");
                    String passconf = request.getParameter("password_confirmation");
                    if(password != null && "".equals(password) == false && password.equals(passconf))
                    {
                        UsuarioDAO usuarioDAO = factory.createUsuarioDAO();
                        usuario = usuarioDAO.consultaPorToken(token);
                        usuario.setSenha(password);
                        try
                        {
                            usuarioDAO.salvar(usuario);
                            tokenDAO.excluir(token.getId());
                        }
                        catch (Exception ex)
                        {
                            throw new ServletException("Não foi possível alterar a senha.");
                        }
                        usuarioDAO.fechar();
                        session.setAttribute("msg", "Senha alterada com sucesso.");
                        request.getRequestDispatcher("/message.jsp").forward(request, response);
                    }
                    else
                    {
                        tokenDAO.excluir(token.getId());
                        throw new ServletException("Senhas não conferem.");
                    }
                }
                else
                {
                    throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
                }

                tokenDAO.fechar();
            }
            else
            {
                throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
            }
        }
        else
        {
            throw new ServletException("Usuário não autorizado a acessar esta funcionalidade.");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }
    // </editor-fold>
}