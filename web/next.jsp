<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <title>Fila de Espera</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/styles.css" type="text/css" />
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <c:import url="navbar.jsp" />
        <div class="container">
            <h2>&nbsp;</h2>
            <form action="NextPacient" method="POST">
                <div class="form-group">
                    <label for="email">Paciente atual</label>
                    <input type="text" class="form-control" id="proximo" name="proximo" value="<c:out value='${paciente.nome}'></c:out>" disabled="disabled" />
                </div>
                <div class="form-group">
                    <label for="email">Ainda h� <c:out value="${r}" /> pacientes na fila a serem atendidos depois deste.</label>
                </div>
                <input type="hidden" name="q" value="${q}" />
                <button type="submit" class="btn btn-default btn-bottom-right" <c:if test="${r <= 0}"> disabled="disabled"</c:if>>Pr�ximo</button>
            </form>
        </div>
    </body>
</html>