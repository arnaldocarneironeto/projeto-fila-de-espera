<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ct" uri="http://devsphere.com/articles/calltag/CallTag.tld" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <title>Fila de Espera</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/styles.css" type="text/css" />
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script>
    </head>
    <body>
        <c:import url="navbar.jsp" />
        <div class="container">
            <h2>&nbsp;</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Usu�rio</th>
                            <th>Fun��o</th>
                            <th>Matr�cula</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="tab-content">
                        <c:forEach var="usuario" items="${usuarios}">
                            <c:if test="${usuario.login != user.login}">
                                <tr>
                                    <td><c:out value="${usuario.nome}" /></td>
                                    <td><c:choose><c:when test="${empty usuario.matricula}">Paciente</c:when><c:otherwise>Atendente</c:otherwise></c:choose></td>
                                    <td><c:out value="${usuario.matricula}" /></td>
                                    <td><a href="PromoteUser/<c:out value='${usuario.id}' ></c:out>" class="btn btn-default" ><span class="glyphicon glyphicon-edit"></span> Editar</a></td>
                                    <td><a href="DeleteUser/<c:out value='${usuario.id}' ></c:out>" class="btn btn-default" ><span class="glyphicon glyphicon-trash"></span> Excluir</a></td>
                                </tr>
                            </c:if>
                        </c:forEach>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </body>
</html>