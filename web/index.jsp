<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ct" uri="http://devsphere.com/articles/calltag/CallTag.tld" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <title>Fila de Espera</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/styles.css" type="text/css" />
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script>
    </head>
    <body>
        <c:import url="navbar.jsp" />
        <div class="container">
            <h2>&nbsp;</h2>
            <c:choose>
                <c:when test="${empty user}">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Dia</th>
                                    <th>M�dico</th>
                                    <th>Especialidades</th>
                                    <th>Hor�rios vagos</th>
                                    <th>Inscritos</th>
                                    <th>Na fila de espera</th>
                                </tr>
                            </thead>
                            <tbody class="tab-content">
                                <c:forEach var="fila" items="${filas}">
                                    <tr>
                                        <ct:call object="${fila}" method="isAntiga" return="isAntiga" />
                                        <ct:call object="${fila}" method="naEspera" return="naEspera" />
                                        <ct:call object="${fila}" method="getDataStr" return="data" />
                                        <c:if test="${isAntiga == false}">
                                            <td><c:out value="${data}" /></td>
                                            <td><c:out value="${fila.medico.nome}" /></td>
                                            <td><c:out value="${fila.medico.especialidade}" /></td>
                                            <td><c:out value="${fila.vagas}" /></td>
                                            <td><c:out value="${fn:length(fila.pacientes)}" /></td>
                                            <td><c:out value="${naEspera}" /></td>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </c:when>
                <c:otherwise>
                    <c:choose>
                        <c:when test="${user.matricula == null}">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Dia</th>
                                            <th>M�dico</th>
                                            <th>Especialidade</th>
                                            <th>Hor�rios vagos</th>
                                            <th>Inscritos</th>
                                            <th>Na fila de espera</th>
                                            <th>Entrar na fila</th>
                                            <th>Sua posi��o</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tab-content">
                                        <c:forEach var="fila" items="${filas}">
                                            <ct:call object="${fila}" method="naEspera" return="naEspera" />
                                            <ct:call object="${fila}" method="getDataStr" return="data" />
                                            <ct:call object="${fila}" method="isAntiga" return="isAntiga" />
                                            <ct:call object="${fila}" method="isHoje" return="isHoje" />
                                            <ct:call object="${fila}" method="getPosicao" paciente="${user}" return="posicao" />
                                            <c:if test="${isAntiga == false}" >
                                                <tr>
                                                    <td><c:out value="${data}" /></td>
                                                    <td><c:out value="${fila.medico.nome}" /></td>
                                                    <td><c:out value="${fila.especialidade}" /></td>
                                                    <td><c:out value="${fila.vagas}" /></td>
                                                    <td><c:out value="${fn:length(fila.pacientes)}" /></td>
                                                    <td><c:out value="${naEspera}" /></td>
                                                    <td><div class="checkbox "><label><input type="checkbox"<c:if test="${posicao > 0}"> checked="checked"</c:if><c:if test="${isHoje}" > disabled="disabled"</c:if><c:if test="${isHoje == false}"> onclick="window.location.assign('EnterLine/<c:out value="${fila.id}" />/')"<c:choose><c:when test="${posicao > 0}"> data-toggle="tooltip" title="Sair da fila"</c:when><c:otherwise> data-toggle="tooltip" title="Entrar na fila"</c:otherwise></c:choose></c:if> /></label></div></td>
                                                    <td><c:if test="${posicao > 0}"><c:out value="${posicao}" />&ordm;</c:if></td>
                                                </tr>
                                            </c:if>
                                        </c:forEach>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="row">
                                <div class="col-sm-6">
                                    <form action="create_line.jsp" method="POST">
                                        <button type="submit" class="btn btn-default">Criar fila de espera</button>
                                    </form>
                                </div>
                                <div class="col-sm-6">
                                    <form action="ManageUsers" method="POST">
                                        <button type="submit" class="btn btn-default">Gerenciar usu�rios</button>
                                    </form>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-linked">
                                    <caption>Filas de hoje</caption>
                                    <thead>
                                        <tr>
                                            <th>Pacientes a serem atendidos</th>
                                            <th>M�dico</th>
                                            <th>Especialidade</th>
                                            <th>Excluir fila?</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tab-content">
                                        <c:forEach var="fila" items="${filas}">
                                            <ct:call object="${fila}" method="isAntiga" return="isAntiga" />
                                            <ct:call object="${fila}" method="isHoje" return="isHoje" />
                                            <ct:call object="${fila}" method="getRestantes" return="restam" />
                                            <c:if test="${isHoje}">
                                                <tr class="clickable-row" data-href="NextPacient?q=<c:out value='${fila.id}' />"<c:if test="${isAntiga == false}">data-toggle="tooltip" title="Ver pr�ximo paciente desta fila"</c:if> >
                                                    <td><c:out value="${restam}" /></td>
                                                    <td><c:out value="${fila.medico.nome}" /></td>
                                                    <td><c:out value="${fila.medico.especialidade}" /></td>
                                                    <td><form action="DeleteLine/<c:out value="${fila.id}" />/" method="POST"><button type="submit" class="btn btn-default" <c:if test="${isAntiga == false}">disabled="disabled"</c:if><c:if test="${isAntiga}">data-toggle="tooltip" title="Excluir esta fila"</c:if> ><span class="glyphicon glyphicon-trash"></span></button></form></td>
                                                    </tr>
                                            </c:if>
                                        </c:forEach>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <caption>Pr�ximos dias</caption>
                                    <thead>
                                        <tr>
                                            <th>Dia</th>
                                            <th>M�dico</th>
                                            <th>Especialidade</th>
                                            <th>Hor�rios vagos</th>
                                            <th>Inscritos</th>
                                            <th>Na fila de espera</th>
                                            <th>Excluir fila?</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tab-content">
                                        <c:forEach var="fila" items="${filas}">
                                            <ct:call object="${fila}" method="isAntiga" return="isAntiga" />
                                            <ct:call object="${fila}" method="isHoje" return="isHoje" />
                                            <ct:call object="${fila}" method="naEspera" return="naEspera" />
                                            <ct:call object="${fila}" method="getDataStr" return="data" />
                                            <c:if test="${isHoje == false}">
                                                <tr>
                                                    <td><c:out value="${data}" /></td>
                                                    <td><c:out value="${fila.medico.nome}" /></td>
                                                    <td><c:out value="${fila.especialidade}" /></td>
                                                    <td><c:out value="${fila.vagas}" /></td>
                                                    <td><c:out value="${fn:length(fila.pacientes)}" /></td>
                                                    <td><c:out value="${naEspera}" /></td>
                                                    <td><form action="DeleteLine/<c:out value="${fila.id}" />/" method="POST"><button type="submit" class="btn btn-default" <c:if test="${isAntiga == false}">disabled="disabled"</c:if><c:if test="${isAntiga}">data-toggle="tooltip" title="Excluir esta fila"</c:if> ><span class="glyphicon glyphicon-trash"></span></button></form></td>
                                                    </tr>
                                            </c:if>
                                        </c:forEach>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </c:otherwise>
            </c:choose>
        </div>
    </body>
</html>