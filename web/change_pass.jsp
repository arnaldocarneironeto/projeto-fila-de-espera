<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <title>Fila de Espera</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/styles.css" type="text/css" />
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <c:import url="navbar.jsp" />
        <!--content-->
        <div class="container">
            <h2>&nbsp;</h2>
            <h2>Nova senha</h2>
            <form action="Recover" method="POST">
                <div class="form-group">
                    <input type="hidden" name="g" value="<c:out value='${usuario.id}'></c:out>$<c:out value='${token.codigo}'></c:out>" />
                </div>
                <div class="form-group">
                    <input type="password" name="password" placeholder="Senha" class="form-control" />
                </div>
                <div class="form-group">
                    <input type="password" name="password_confirmation" placeholder="Confirmar senha" class="form-control" />
                </div>
                <button type="submit" class="btn btn-default btn-group-justified"><span class="glyphicon glyphicon-send"></span> Enviar</button>
            </form>
        </div>
    </body>
</html>