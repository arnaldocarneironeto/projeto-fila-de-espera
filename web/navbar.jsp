<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <div class="container example2">
            <div class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Main"><img src="img/logo.gif" alt="Fila de Espera"></a>
                    </div>
                    <div class="collapse navbar-collapse" id="mainNavbar">
                        <c:choose>
                            <c:when test="${empty user}">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Entrar <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li class="dropdown-header">J� possui cadastro?</li>
                                            <li>
                                                <form  action="Login" method="post">
                                                    <div class="form-group">
                                                        <input type="text" placeholder="Nome de usu�rio" name="login" id="login" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="password" placeholder="Senha" name="password" id="password" />
                                                    </div>
                                                    <a href="password_recovery.jsp" >Esqueceu a senha?</a>
                                                    <button type="submit" class="btn btn-default btn-group-justified"><span class="glyphicon glyphicon-log-in"></span> Entrar</button>
                                                </form>
                                            </li>
                                            <li class="divider"></li>
                                            <li class="dropdown-header">Ainda n�o possui cadastro?</li>
                                            <li>
                                                <form  action="registerform.jsp" method="post">
                                                    <button type="submit" class="btn btn-default btn-group-justified"><span class="glyphicon glyphicon-user"></span> Cadastre-se</button>
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </c:when>
                            <c:otherwise>
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Ol�, <c:out value="${user.nome}"></c:out> <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="updateform.jsp" >Atualizar dados</a></li>
                                                <li class="divider"></li>
                                                <li><a href="Logoff" ><span class="glyphicon glyphicon-log-out"></span> Sair</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </div>