<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.arnaldocarneiro.model.Usuario"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <title>Fila de Espera</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/styles.css" type="text/css" />
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <% Usuario user = (Usuario) session.getAttribute("user");%>
    </head>
    <body>
        <c:import url="navbar.jsp" />
        <!--content-->
        <div class="container">
            <h2>&nbsp;</h2>
            <h2><c:out value="${msg}"></c:out></h2>
        </div>
    </body>
</html>