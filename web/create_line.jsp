<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <title>Fila de Espera</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/bootstrap-datepicker3.min.css" type="text/css" />
        <link rel="stylesheet" href="css/styles.css" type="text/css" />
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-datepicker.min.js"></script>
        <script>
            $(document).ready(function ()
            {
                $('#date').datepicker({format: "dd/mm/yyyy",
                    startDate: "<c:out value='${amanha}' />",
                    language: "pt-BR"});
            });
        </script>
    </head>
    <body>
        <c:import url="navbar.jsp" />
        <!--content-->
        <div class="container">
            <h2>&nbsp;</h2>
            <form action="CreateLine" method="POST"">
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label>M�dico:</label>
                            <select name="doctor" class="form-control">
                                <c:forEach var="medico" items="${medicos}">
                                    <option value='<c:out value="${medico.nome}" />'><c:out value="${medico.nome}" /> (<c:out value="${medico.especialidade}" />)</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Vagas dispon�veis</label>
                            <input type="number" class="form-control" id="spots" name="spots" placeholder="N�mero de vagas" min="1" max="28" />
                        </div>
                        <button type="submit" class="btn btn-default">Criar fila</button>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Data</label>
                            <input type="text" class="form-control" id="date" name="date" placeholder="dd/mm/aaaa" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!--content end-->
    </body>
</html>