<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <title>Fila de Espera</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/styles.css" type="text/css" />
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <c:import url="navbar.jsp" />
        <!--content-->
        <div class="container">
            <h2>&nbsp;</h2>
            <h2>Mudar tipo de usu�rio: <em><c:out value="${usuario.nome}"></c:out></em></h2>
            <c:choose>
                <c:when test="${empty usuario.matricula}">
                    <form action="ManageUser" method="POST">
                        <input type="hidden" name="userid" value="<c:out value='${usuario.id}'></c:out>" />
                        <input type="hidden" name="usertype" value="atendente" />
                        <div class="form-group">
                            <input type="text" name="matricula" placeholder="Matr�cula" class="form-control" />
                        </div>
                        <button type="submit" class="btn btn-default btn-group-justified"><span class="glyphicon glyphicon-user"></span><span class="glyphicon glyphicon-refresh"></span> Promover a Atendente</button>
                    </form>
                </c:when>
                <c:otherwise>
                    <form action="ManageUser" method="POST">
                        <input type="hidden" name="userid" value="<c:out value='${usuario.id}'></c:out>" />
                        <input type="hidden" name="usertype" value="paciente" />
                        <button type="submit" class="btn btn-default btn-group-justified"><span class="glyphicon glyphicon-user"></span><span class="glyphicon glyphicon-refresh"></span> Rebaixar a Paciente</button>
                    </form>
                </c:otherwise>
            </c:choose>
        </div>
    </body>
</html>