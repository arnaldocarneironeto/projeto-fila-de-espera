<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <title>Fila de Espera</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/styles.css" type="text/css" />
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <c:import url="navbar.jsp" />
        <!--content-->
        <div class="container">
            <h2>&nbsp;</h2>
            <h2>Esqueceu a senha?</h2>
            <p>Informe o seu email registrado ou o seu nome de usu�rio que dentro de instantes n�s enviaremos um email com as informa��es de recuper��o ao seu email registrado.</p>
            <form action="PassRecover" method="POST">
                <div class="form-group">
                    <input type="text" name="login" placeholder="Nome de usu�rio" class="form-control" />
                </div>
                <p>ou</p>
                <div class="form-group">
                    <input type="email" name="email" placeholder="E-mail" class="form-control" />
                </div>
                <button type="submit" class="btn btn-default btn-group-justified"><span class="glyphicon glyphicon-send"></span> Enviar</button>
            </form>
        </div>
    </body>
</html>